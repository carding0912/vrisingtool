# VRisingtool

#### 介绍
游戏VRising Windows server服务端架设全自动工具

#### 软件架构
Python3.9 + PySide6.4.2 + MySQL8.0

#### 安装教程
第三方包截图

![三方包截图](https://foruda.gitee.com/images/1678252503955300609/d8dada00_12201524.png "tools.png")

1、Python3.9

2、PySide6.4.2

3、MySQL8.0

4、pillow

5、pymysql

6、pywin32

7、pyinstaller(项目为可视化窗口，打包用)

#### 使用说明
要安装和配置MySQL数据库，提供登陆验证（数据库登陆验证部分代码需要修改，没有使用PySide6内置验证器）

