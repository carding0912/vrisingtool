from PySide6 import QtGui
from PySide6.QtCore import Qt, QSize, QRect, QMetaObject
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QMessageBox, QApplication, QMainWindow
from images import images
import sys
from service import service
import main


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        MainWindow.resize(410, 300)
        font = QtGui.QFont()
        font.setBold(False)
        MainWindow.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icon/icon.jpg"), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setIconSize(QSize(32, 32))
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setStyleSheet("")
        self.centralwidget.setObjectName("centralwidget")
        self.label = QLabel(self.centralwidget)
        self.label.setGeometry(QRect(0, 0, 411, 161))
        self.label.setStyleSheet("border-image: url(:/login/login.jpg);")
        self.label.setText("")
        self.label.setObjectName("label")
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setGeometry(QRect(130, 180, 61, 21))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(False)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.editName = QLineEdit(self.centralwidget)
        self.editName.setGeometry(QRect(210, 180, 141, 20))
        self.editName.setObjectName("editName")
        self.editPwd = QLineEdit(self.centralwidget)
        self.editPwd.setGeometry(QRect(210, 220, 141, 20))
        self.editPwd.setEchoMode(QLineEdit.EchoMode.Password)
        self.editPwd.setObjectName("editPwd")
        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setGeometry(QRect(130, 220, 61, 21))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(False)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.btnLogin = QPushButton(self.centralwidget)
        self.btnLogin.setGeometry(QRect(210, 260, 61, 23))
        self.btnLogin.setObjectName("btnLogin")
        self.btnExit = QPushButton(self.centralwidget)
        self.btnExit.setGeometry(QRect(290, 260, 61, 23))
        self.btnExit.setObjectName("btnExit")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.btnExit.clicked.connect(MainWindow.close)
        QMetaObject.connectSlotsByName(MainWindow)
        # 输入密码后按回车键执行登录操作
        self.editPwd.returnPressed.connect(self.openMain)
        # self.editPwd.editingFinished.connect(self.openMain)  #  会触发两次回馈值!
        # 单击“登录”按钮执行登录操作
        self.btnLogin.clicked.connect(self.openMain)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("V Rising Server Tools")
        self.label_2.setText("用户名：")
        self.label_3.setText("密  码：")
        self.btnLogin.setText("登录")
        self.btnExit.setText("退出")

    def openMain(self):
        self.btnLogin.setEnabled(0)
        service.username = self.editName.text()  # 全局变量，记录用户名
        self.password = self.editPwd.text()  # 记录用户密码
        if service.username != "" and self.password != "":  # 判断用户名和密码不为空
            # 根据用户名和密码查询数据
            result = service.query("select * from tb_users where username = %s and password = %s",
                                   service.username, self.password)
            if len(result) > 0:  # 如果查询结果大于0，说明存在该用户，可以登录
                # 登录用户权限提示
                if result[0][2] == 8888:
                    QMessageBox.about(MainWindow, '欢迎登录:', '尊敬的VIP会员,开发不易,感谢您的支持！')

                else:
                    QMessageBox.about(MainWindow, '欢迎登录:', '您是注册用户,开发不易,感谢您的使用！')

                self.m = main.Ui_MainWindow()  # 创建主窗体对象
                self.m.show()  # 显示主窗体
                MainWindow.hide()  # 隐藏当前的登录窗体
                self.btnLogin.setEnabled(1)
            else:
                self.editName.setText("")  # 清空用户名文本
                self.editPwd.setText("")  # 清空密码文本框
                QMessageBox.about(MainWindow, '警告', '请输入正确的用户名和密码！')
                self.btnLogin.setEnabled(1)
        else:
            QMessageBox.warning(MainWindow, '警告', '请输入用户名和密码！', QMessageBox.StandardButton.Yes,
                                QMessageBox.StandardButton.Yes)
            self.btnLogin.setEnabled(1)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程
