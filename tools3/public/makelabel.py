from PySide6.QtCore import QRect, QTime
from PySide6.QtGui import QFont
from PySide6.QtWidgets import QLabel, QSpinBox, QComboBox, QDoubleSpinBox, QTimeEdit, QTextEdit

class CheckLabel:
    """
    解锁BOSS设置的类
    """
    f2 = QFont()
    f2.setBold(False)
    f2.setPointSize(10)
    f3 = QFont()
    f3.setBold(False)
    f3.setPointSize(11)
    f4 = QFont()
    f4.setBold(True)
    f4.setPointSize(11)

    def __init__(self,f_case,en_name,x,y,zh_name,de_level):
        """
         {
      "UnitId": -1905691330,
      "UnitLevel": 16,
      "DefaultUnlocked": true
       },
        :param f_case: 上级窗口名
        :param en_name: "UnitID"  boss的ID
        :param x:  复选框所在位置x轴
        :param y:  所在位置y轴
        :param zh_name: 复选框后的文字
        :param de_level: Boss的默认等级
        """
        self.de_level = de_level
        self.f_case = f_case
        self.en_name = en_name
        self.x = x
        self.y = y
        self.zh_name = zh_name
        self.make_label()
        self.changelevel_spin()
        self.make_comb()

    def make_label(self):
        self.label = QLabel(self.f_case)
        self.label.setGeometry(QRect(self.x,self.y,120,25))
        self.label.setObjectName(self.en_name)
        self.label.setText(self.zh_name)
        self.label.setFont(self.f4)
        self.label.setStyleSheet(u"color: rgb(0, 0, 0);")

    def changelevel_spin(self):
        self.spinBox = QSpinBox(self.f_case)
        self.spinBox.setGeometry(QRect(self.x + 120, self.y, 50, 25))
        self.spinBox.setObjectName('UnitLevel')
        self.spinBox.setMinimum(0)
        self.spinBox.setMaximum(80)
        self.spinBox.setProperty('value', self.de_level)
        self.spinBox.setSingleStep(1)
        self.spinBox.setFont(self.f2)
        self.spinBox.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.spinBox.editingFinished.connect(self.color_changed)

    def make_comb(self):
        self.comboBox = QComboBox(self.f_case)
        self.comboBox.setGeometry(QRect(self.x + 175, self.y,65, 25))
        self.comboBox.setObjectName(self.en_name)
        self.comboBox.addItem('解锁')
        self.comboBox.addItem('不解锁')
        self.comboBox.setFont(self.f2)
        self.comboBox.setCurrentIndex(1)
        self.comboBox.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.comboBox.highlighted.connect(self.color_changed)

    def color_changed(self):
        self.label.setFont(self.f4)
        self.spinBox.setFont(self.f4)
        self.comboBox.setFont(self.f4)
        # self.checkBox.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.spinBox.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.comboBox.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.label.setStyleSheet(u"color: rgb(0, 0, 255);")

    def result(self):
        res1 = '"UnitId": ' + self.label.objectName() + ',\n' + '      "UnitLevel": ' + str(self.spinBox.value()) + ',\n'
        res2 ='      "DefaultUnlocked": true' if self.comboBox.currentIndex() == 0 else '      "DefaultUnlocked": false'
        res = '    {\n      ' + res1 + res2 + '\n    },\n'
        return res


class CombLabel:
    f2 = QFont()
    f2.setBold(False)
    f2.setPointSize(10)
    f3 = QFont()
    f3.setBold(False)
    f3.setPointSize(11)

    def __init__(self,f_case,x,y,en_name,labeltext,comb_list,show_list):
        self.f_case = f_case
        self.x = x
        self.y = y
        self.en_name = en_name
        self.labeltext = labeltext
        self.comb_list = comb_list
        self.show_list = show_list
        self.make_label()
        self.make_comb()


    def make_label(self):
        self.label = QLabel(self.f_case)
        self.label.setGeometry(QRect(self.x,self.y,160,25))
        self.label.setObjectName(self.en_name)
        self.label.setText(self.labeltext)
        self.label.setFont(self.f2)
        self.label.setStyleSheet(u"color: rgb(0, 0, 0);")

    def make_comb(self):
        self.comboBox = QComboBox(self.f_case)
        self.comboBox.setGeometry(QRect(self.x + 150, self.y, 160, 25))
        self.comboBox.setObjectName(self.en_name)
        self.comboBox.addItems(self.comb_list)
        self.comboBox.setFont(self.f2)
        self.comboBox.setStyleSheet(u"color: rgb(0, 0, 0);")
        for i in range(len(self.comb_list)):
            self.comboBox.setItemText(i,self.show_list[i])
        self.comboBox.highlighted.connect(self.color_changed)


    def color_changed(self):
        self.comboBox.setFont(self.f3)
        self.label.setFont(self.f3)
        self.comboBox.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.label.setStyleSheet(u"color: rgb(0, 0, 255);")

    def result(self):
        res1 = '"' + self.label.objectName() + '": '
        res2 = self.comb_list[self.comboBox.currentIndex()]
        if res2 not in ['true','false',]:
            res2 = '"' + res2 + '"'
        if res2.endswith(']"'):
            res2 = res2[1:-1]
        res = res1 + res2
        return res

class SpinLabel:
    f2 = QFont()
    f2.setBold(False)
    f2.setPointSize(10)
    f3 = QFont()
    f3.setBold(False)
    f3.setPointSize(11)

    def __init__(self,f_case,x,y,en_name,labeltext,value,mi,ma,step):
        self.f_case = f_case
        self.x = x
        self.y = y
        self.en_name = en_name
        self.labeltext = labeltext
        self.value = value
        self.mi = mi
        self.ma = ma
        self.step = step
        self.make_label()
        self.make_spin()

    def make_label(self):
        self.label = QLabel(self.f_case)
        self.label.setGeometry(QRect(self.x,self.y,160,25))
        self.label.setObjectName(self.en_name)
        self.label.setText(self.labeltext)
        self.label.setStyleSheet(u"color: rgb(0, 0, 0);")
        f2 = QFont()
        f2.setBold(False)
        f2.setPointSize(10)
        self.label.setFont(f2)

    def make_spin(self):
        self.spinBox = QSpinBox(self.f_case)
        self.spinBox.setGeometry(QRect(self.x + 150, self.y, 160, 25))
        self.spinBox.setObjectName(self.en_name)
        self.spinBox.setMinimum(self.mi)
        self.spinBox.setMaximum(self.ma)
        self.spinBox.setProperty('value',self.value)
        self.spinBox.setSingleStep(self.step)
        self.spinBox.setFont(self.f2)
        self.spinBox.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.spinBox.editingFinished.connect(self.color_changed)

    def color_changed(self):
        self.spinBox.setFont(self.f3)
        self.label.setFont(self.f3)
        self.spinBox.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.label.setStyleSheet(u"color: rgb(0, 0, 255);")

    def result(self):
        res1 = '"' + self.label.objectName() + '": '
        res = res1 + str(self.spinBox.value())
        return res


class DoubleLabel:
    f2 = QFont()
    f2.setBold(False)
    f2.setPointSize(10)
    f3 = QFont()
    f3.setBold(False)
    f3.setPointSize(11)

    def __init__(self, f_case, x, y, en_name, labeltext, value, mi, ma, step, de):
        self.f_case = f_case
        self.x = x
        self.y = y
        self.en_name = en_name
        self.labeltext = labeltext
        self.value = value
        self.mi = mi
        self.ma = ma
        self.step = step
        self.de = de
        self.make_label()
        self.make_double()

    def make_label(self):
        self.label = QLabel(self.f_case)
        self.label.setGeometry(QRect(self.x, self.y, 160, 25))
        self.label.setObjectName(self.en_name)
        self.label.setText(self.labeltext)
        f2 = QFont()
        f2.setBold(False)
        f2.setPointSize(10)
        self.label.setFont(f2)
        self.label.setStyleSheet(u"color: rgb(0, 0, 0);")

    def make_double(self):
        self.doubleSpinBox = QDoubleSpinBox(self.f_case)
        self.doubleSpinBox.setGeometry(QRect(self.x + 150, self.y, 160, 25))
        self.doubleSpinBox.setObjectName(self.en_name)
        self.doubleSpinBox.setMinimum(self.mi)
        self.doubleSpinBox.setMaximum(self.ma)
        self.doubleSpinBox.setSingleStep(self.step)
        self.doubleSpinBox.setProperty('value',self.value)
        self.doubleSpinBox.setDecimals(self.de)
        self.doubleSpinBox.setFont(self.f2)
        self.doubleSpinBox.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.doubleSpinBox.editingFinished.connect(self.color_changed)

    def color_changed(self):
        self.doubleSpinBox.setFont(self.f3)
        self.label.setFont(self.f3)
        self.doubleSpinBox.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.label.setStyleSheet(u"color: rgb(0, 0, 255);")

    def result(self):
        res1 = '"' + self.label.objectName() + '": '
        res = res1 + str(round(self.doubleSpinBox.value(),self.de))
        return res

class TimeLabel:
    f2 = QFont()
    f2.setBold(False)
    f2.setPointSize(10)
    f3 = QFont()
    f3.setBold(False)
    f3.setPointSize(11)

    def __init__(self, f_case, x, y, en_name, labeltext, value,):
        self.f_case = f_case
        self.x = x
        self.y = y
        self.en_name = en_name
        self.labeltext = labeltext
        self.value = value
        self.make_label()
        self.make_time()

    def make_label(self):
        self.label = QLabel(self.f_case)
        self.label.setGeometry(QRect(self.x, self.y, 160, 25))
        self.label.setObjectName(self.en_name)
        self.label.setText(self.labeltext)
        f2 = QFont()
        f2.setBold(False)
        f2.setPointSize(10)
        self.label.setFont(f2)
        self.label.setStyleSheet(u"color: rgb(0, 0, 0);")

    def make_time(self):
        self.timeEdit = QTimeEdit(self.f_case)
        self.timeEdit.setGeometry(QRect(self.x + 150, self.y, 160, 25))
        self.timeEdit.setTime(QTime(*self.value))
        self.timeEdit.setDisplayFormat('H:m')
        self.timeEdit.setFont(self.f2)
        self.timeEdit.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.timeEdit.editingFinished.connect(self.color_changed)

    def color_changed(self):
        self.timeEdit.setFont(self.f3)
        self.label.setFont(self.f3)
        self.timeEdit.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.label.setStyleSheet(u"color: rgb(0, 0, 255);")

    def result(self):
        res1 = '"' + self.label.objectName() + '": '
        res = res1 + self.timeEdit.text()
        return res

class TextLabel:
    f2 = QFont()
    f2.setBold(False)
    f2.setPointSize(10)
    f3 = QFont()
    f3.setBold(False)
    f3.setPointSize(11)

    def __init__(self,f_case,x,y,en_name,labeltext,value):
        self.f_case = f_case
        self.x = x
        self.y = y
        self.en_name = en_name
        self.labeltext = labeltext
        self.value = value
        self.make_label()
        self.make_text()

    def make_label(self):
        self.label = QLabel(self.f_case)
        self.label.setGeometry(QRect(self.x,self.y,160,25))
        self.label.setObjectName(self.en_name)
        self.label.setText(self.labeltext)
        self.label.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.label.setFont(self.f2)

    def make_text(self):
        self.textedit = QTextEdit(self.f_case)
        self.textedit.setGeometry(QRect(self.x + 150, self.y, 160, 25))
        # self.textedit.setTextColor(QtGui.QColor(0,0,255))
        self.textedit.setPlainText(self.value)
        self.textedit.setFont(self.f2)
        self.textedit.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.textedit.selectionChanged.connect(self.color_changed)

    def color_changed(self):
        self.textedit.setFont(self.f2)
        self.label.setFont(self.f3)
        self.textedit.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.label.setStyleSheet(u"color: rgb(0, 0, 255);")

    def result(self):
        res1 = '"' + self.label.objectName() + '": '
        res2 = self.textedit.toPlainText()
        res2 = '"' + res2 + '"'
        res = res1 + res2
        return res
