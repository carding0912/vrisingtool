# -*- coding: utf-8 -*-
import sys
import about
from PySide6 import QtWidgets
from PySide6.QtCore import Qt, QSize, QRect, QDateTime, QMetaObject
from PySide6.QtGui import QIcon, QPixmap, QFont, QAction
from PySide6.QtWidgets import QMessageBox
from dataaction import datasave, backdata
from gamesettings import personalset
from manage import gmmanage, banmanage
from serverstart import start
from service import service
from serverclientupdata import automaticdownload, clientupdate
from gamesettings import official
from hostsetting import serverset


class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        icon = QIcon()
        icon.addPixmap(QPixmap(":/icon/icon.jpg"), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setIconSize(QSize(32, 32))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QRect(100, 80, 531, 51))
        self.label.setObjectName("label")

        MainWindow.setCentralWidget(self.centralwidget)
        # 创建菜单栏
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QRect(0, 0, 800, 30))
        font = QFont()
        font.setPointSize(11)
        self.menubar.setFont(font)
        self.menubar.setObjectName("菜单栏")
        # 在菜单栏中创建主菜单
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("游戏服务器端")
        self.menu_2 = QtWidgets.QMenu(self.menubar)
        self.menu_2.setObjectName("游戏参数设置")
        self.menu_3 = QtWidgets.QMenu(self.menubar)
        self.menu_3.setObjectName("服务器参数设置")
        self.menu_4 = QtWidgets.QMenu(self.menubar)
        self.menu_4.setObjectName("启动服务器")
        self.menu_5 = QtWidgets.QMenu(self.menubar)
        self.menu_5.setObjectName("数据备份")
        self.menu_6 = QtWidgets.QMenu(self.menubar)
        self.menu_6.setObjectName("游戏实时管理")
        self.menu_7 = QtWidgets.QMenu(self.menubar)
        self.menu_7.setObjectName("关于本工具")
        self.menu_8 = QtWidgets.QMenu(self.menubar)
        self.menu_8.setObjectName("退出使用")
        # 添加菜单栏到主窗口
        MainWindow.setMenuBar(self.menubar)
        # 创建底部状态栏
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        font = QFont()
        font.setPointSize(10)
        self.statusbar.setFont(font)
        self.statusbar.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(0, 0, 255);")
        self.statusbar.setSizeGripEnabled(False)
        MainWindow.setStatusBar(self.statusbar)
        # 底部状态栏添加用户信息显示
        datetime = QDateTime.currentDateTime()  # 获取当前日期时间
        time = datetime.toString("yyyy-MM-dd HH:mm:ss")  # 对日期时间进行格式化
        # 状态栏中显示登录用户、登录时间，以及版权信息
        self.labelinfo = QtWidgets.QLabel()
        self.labelinfo.setText(
            "当前登录用户：" + service.username + '    欢迎您的使用' + "     登录时间：" + time + "      版权所有：微信Python团助群")
        self.statusbar.addWidget(self.labelinfo)
        # 创建子菜单
        self.server_client = QAction(MainWindow)
        self.server_client.setObjectName("全自动生成服务器端")

        self.server_client_updata = QAction(MainWindow)
        self.server_client_updata.setObjectName("游戏更新")

        self.official_set = QAction(MainWindow)
        self.official_set.setObjectName("启用官方游戏配置")

        self.personal_set = QAction(MainWindow)
        self.personal_set.setObjectName("自定义游戏配置")

        self.server_set = QAction(MainWindow)
        self.server_set.setObjectName("解析/公告/端口/标题设置")

        self.server_start = QAction(MainWindow)
        self.server_start.setObjectName("服务器的启动")

        self.server_data = QAction(MainWindow)
        self.server_data.setObjectName("服务器和游戏参数备份")

        self.player_data = QAction(MainWindow)
        self.player_data.setObjectName("玩家游戏数据备份")

        self.gamemaster_set = QAction(MainWindow)
        self.gamemaster_set.setObjectName("添加游戏管理员")
        self.ban_set = QAction(MainWindow)
        self.ban_set.setObjectName("设置游戏黑名单")
        self.game_notice = QAction(MainWindow)
        self.game_notice.setObjectName("游戏实时公告")
        self.about_tools = QAction(MainWindow)
        self.about_tools.setObjectName("开发运营相关说明")
        self.exit_tools = QAction(MainWindow)
        self.exit_tools.setObjectName("点击退出")
        self.back_data = QAction(MainWindow)
        self.back_data.setObjectName("点击退出")

        self.menu.addSeparator()
        self.menu.addAction(self.server_client)
        self.menu.addSeparator()
        self.menu.addAction(self.server_client_updata)
        self.menu.addSeparator()
        self.menu_2.addSeparator()
        self.menu_2.addAction(self.official_set)
        self.menu_2.addSeparator()
        self.menu_2.addAction(self.personal_set)
        self.menu_2.addSeparator()
        self.menu_3.addSeparator()
        self.menu_3.addAction(self.server_set)
        self.menu_3.addSeparator()
        self.menu_4.addSeparator()
        self.menu_4.addAction(self.server_start)
        self.menu_4.addSeparator()
        self.menu_5.addSeparator()
        self.menu_5.addAction(self.server_data)
        self.menu_5.addSeparator()
        self.menu_5.addAction(self.player_data)
        self.menu_5.addSeparator()
        self.menu_6.addSeparator()
        self.menu_6.addAction(self.gamemaster_set)
        self.menu_6.addSeparator()
        self.menu_6.addAction(self.ban_set)
        self.menu_6.addSeparator()
        self.menu_6.addAction(self.game_notice)
        self.menu_6.addSeparator()
        self.menu_7.addSeparator()
        self.menu_7.addAction(self.about_tools)
        self.menu_7.addSeparator()
        self.menu_8.addSeparator()
        self.menu_8.addAction(self.exit_tools)
        self.menu_8.addSeparator()
        self.menu_5.addAction(self.back_data)
        self.menu_5.addSeparator()
        self.menubar.addAction(self.menu.menuAction())
        self.menubar.addAction(self.menu_2.menuAction())
        self.menubar.addAction(self.menu_3.menuAction())
        self.menubar.addAction(self.menu_4.menuAction())
        self.menubar.addAction(self.menu_5.menuAction())
        self.menubar.addAction(self.menu_6.menuAction())
        self.menubar.addAction(self.menu_7.menuAction())
        self.menubar.addAction(self.menu_8.menuAction())

        self.retranslateUi(MainWindow)
        self.server_client.triggered.connect(self.automatic)
        self.server_client_updata.triggered.connect(self.client_update)
        self.official_set.triggered.connect(self.officialsett)
        self.personal_set.triggered.connect(self.personalset)
        self.server_set.triggered.connect(self.serverset)
        self.server_start.triggered.connect(self.serverstart)
        self.gamemaster_set.triggered.connect(self.gmmanage)
        self.ban_set.triggered.connect(self.banmanage)
        self.about_tools.triggered.connect(self.abouttools)
        self.server_data.triggered.connect(self.datasave)
        self.player_data.triggered.connect(self.datasave)
        self.game_notice.triggered.connect(self.game_noticefun)
        self.back_data.triggered.connect(self.back_game_data)

        self.exit_tools.triggered.connect(MainWindow.close)  # 退出子菜单绑定信号
        QMetaObject.connectSlotsByName(MainWindow)

    def back_game_data(self):
        self.set10 = backdata.Ui_MainWindow()
        self.set10.show()


    def game_noticefun(self):
        QMessageBox.information(self,'功能提示:','此功能集成测试中,暂未开放.')

    def datasave(self):
        self.set9 = datasave.Ui_MainWindow()
        self.set9.show()

    def gmmanage(self):
        self.set6 = gmmanage.Ui_MainWindow()
        self.set6.show()

    def abouttools(self):
        self.set8 = about.Ui_MainWindow()
        self.set8.show()

    def banmanage(self):
        self.set7 = banmanage.Ui_MainWindow()
        self.set7.show()

    def client_update(self):
        self.set = clientupdate.Ui_MainWindow()
        self.set.show()

    def automatic(self):
        self.set1 = automaticdownload.Ui_MainWindow()
        self.set1.show()

    def officialsett(self):
        self.set2 = official.Ui_MainWindow()
        self.set2.show()

    def personalset(self):
        self.set3 = personalset.Ui_MainWindow()
        self.set3.show()

    def serverset(self):
        self.set4 = serverset.Ui_MainWindow()
        self.set4.show()

    def serverstart(self):
        self.set5 = start.Ui_MainWindow()
        self.set5.show()

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("VRising开服工具")
        self.label.setText("工具使用说明:(待添加)")
        self.menu.setTitle("游戏服务器端")
        self.menu_2.setTitle("游戏参数设置")
        self.menu_3.setTitle("服务器参数设置")
        self.menu_4.setTitle("启动服务器")
        self.menu_5.setTitle("数据备份")
        self.menu_6.setTitle("游戏实时管理")
        self.menu_7.setTitle("关于本工具")
        self.menu_8.setTitle("退出使用")
        self.server_client.setText("全自动生成服务端")
        self.server_client_updata.setText("游戏更新")
        self.official_set.setText("官方默认的设置")
        self.personal_set.setText("自定义参数设置")
        self.server_set.setText("解析/公告/端口/标题设置")
        self.server_start.setText("服务器的启动")
        self.server_data.setText("服务器和游戏参数备份")
        self.player_data.setText("玩家游戏数据备份")
        self.gamemaster_set.setText("游戏管理员管理系统")
        self.ban_set.setText("游戏黑名单管理系统")
        self.game_notice.setText("游戏实时公告(RCON)")
        self.about_tools.setText("开发运营相关说明")
        self.exit_tools.setText("点击退出")
        self.back_data.setText("回滚玩家游戏数据")


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程
