# -*- coding: utf-8 -*-


import pymysql  # 导入操作MySQL数据库的模块

# 打开数据库连接
def opendb():
    db = pymysql.connect(host="localhost", port=3306, user="username",
                         password="password", database="db_vrising",charset="utf8")
    return db  # 返回连接对象

username = ''
# 带参数的精确查询
def query(sql,*keys):
    db=opendb()  # 连接数据库
    cursor = db.cursor()  # 使用cursor()方法获取操作游标
    cursor.execute(sql,keys)  # 执行查询SQL语句
    result = cursor.fetchall()  # 记录查询结果
    cursor.close()  # 关闭游标
    db.close()  # 关闭数据库连接
    return result  # 返回查询结果

if __name__ == '__main__':
    username = 'vipuser'
    password = '123456'
    result = query("select * from tb_users where username = %s and password = %s",username,password)
    result1 = query("select * from tb_users where username = %s",username)
    print(result1)





