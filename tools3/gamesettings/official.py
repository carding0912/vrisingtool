import sys
from PySide6.QtCore import Qt, QRect, QMetaObject
from PySide6.QtGui import QIcon, QPixmap, QFont
from PySide6.QtWidgets import QWidget, QGroupBox, QPushButton, QMessageBox, QApplication, QMainWindow
from images import images
import offcial_codes


class Ui_MainWindow(QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        icon = QIcon()
        icon.addPixmap(QPixmap(":/set/set.jpg"), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QRect(60, 30, 701, 191))
        font = QFont()
        font.setFamily("微软雅黑")
        font.setPointSize(14)
        self.groupBox.setFont(font)
        self.groupBox.setStyleSheet("color: rgb(0, 0, 255);")
        self.groupBox.setObjectName("groupBox")
        self.pushButton = QPushButton(self.groupBox)
        self.pushButton.setGeometry(QRect(30, 50, 141, 41))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QPushButton(self.groupBox)
        self.pushButton_2.setGeometry(QRect(190, 50, 141, 41))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QPushButton(self.groupBox)
        self.pushButton_3.setGeometry(QRect(350, 50, 141, 41))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_4 = QPushButton(self.groupBox)
        self.pushButton_4.setGeometry(QRect(520, 50, 141, 41))
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_5 = QPushButton(self.groupBox)
        self.pushButton_5.setGeometry(QRect(30, 120, 141, 41))
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_6 = QPushButton(self.groupBox)
        self.pushButton_6.setGeometry(QRect(190, 120, 141, 41))
        self.pushButton_6.setObjectName("pushButton_6")
        self.pushButton_7 = QPushButton(self.groupBox)
        self.pushButton_7.setGeometry(QRect(350, 120, 141, 41))
        self.pushButton_7.setObjectName("pushButton_7")
        self.groupBox_2 = QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QRect(60, 280, 701, 271))
        font = QFont()
        font.setFamily("微软雅黑")
        font.setPointSize(14)
        self.groupBox_2.setFont(font)
        self.groupBox_2.setStyleSheet("color: rgb(0, 0, 255);")
        self.groupBox_2.setObjectName("groupBox_2")
        self.pushButton_8 = QPushButton(self.groupBox_2)
        self.pushButton_8.setGeometry(QRect(30, 50, 141, 41))
        self.pushButton_8.setObjectName("pushButton_8")
        self.pushButton_9 = QPushButton(self.groupBox_2)
        self.pushButton_9.setGeometry(QRect(190, 50, 141, 41))
        self.pushButton_9.setObjectName("pushButton_9")
        self.pushButton_10 = QPushButton(self.groupBox_2)
        self.pushButton_10.setGeometry(QRect(350, 50, 141, 41))
        self.pushButton_10.setObjectName("pushButton_10")
        self.pushButton_11 = QPushButton(self.groupBox_2)
        self.pushButton_11.setGeometry(QRect(520, 50, 141, 41))
        self.pushButton_11.setObjectName("pushButton_11")
        self.pushButton_12 = QPushButton(self.groupBox_2)
        self.pushButton_12.setGeometry(QRect(30, 120, 141, 41))
        self.pushButton_12.setObjectName("pushButton_12")
        self.pushButton_13 = QPushButton(self.groupBox_2)
        self.pushButton_13.setGeometry(QRect(190, 120, 141, 41))
        self.pushButton_13.setObjectName("pushButton_13")
        self.pushButton_14 = QPushButton(self.groupBox_2)
        self.pushButton_14.setGeometry(QRect(350, 120, 141, 41))
        self.pushButton_14.setObjectName("pushButton_14")
        self.pushButton_15 = QPushButton(self.groupBox_2)
        self.pushButton_15.setGeometry(QRect(520, 120, 141, 41))
        self.pushButton_15.setObjectName("pushButton_15")
        self.pushButton_16 = QPushButton(self.groupBox_2)
        self.pushButton_16.setGeometry(QRect(30, 180, 141, 41))
        self.pushButton_16.setObjectName("pushButton_16")
        self.pushButton_17 = QPushButton(self.groupBox_2)
        self.pushButton_17.setGeometry(QRect(190, 180, 141, 41))
        self.pushButton_17.setObjectName("pushButton_17")
        self.pushButton_18 = QPushButton(self.groupBox_2)
        self.pushButton_18.setGeometry(QRect(350, 180, 141, 41))
        self.pushButton_18.setObjectName("pushButton_18")
        MainWindow.setCentralWidget(self.centralwidget)

        self.pushButton.clicked.connect(self.choose1)
        self.pushButton_2.clicked.connect(self.choose2)
        self.pushButton_3.clicked.connect(self.choose3)
        self.pushButton_4.clicked.connect(self.choose4)
        self.pushButton_5.clicked.connect(self.choose5)
        self.pushButton_6.clicked.connect(self.choose6)
        self.pushButton_7.clicked.connect(self.choose7)
        self.pushButton_8.clicked.connect(self.choose8)
        self.pushButton_9.clicked.connect(self.choose9)
        self.pushButton_10.clicked.connect(self.choose10)
        self.pushButton_11.clicked.connect(self.choose11)
        self.pushButton_12.clicked.connect(self.choose12)
        self.pushButton_13.clicked.connect(self.choose13)
        self.pushButton_14.clicked.connect(self.choose14)
        self.pushButton_15.clicked.connect(self.choose15)
        self.pushButton_16.clicked.connect(self.choose16)
        self.pushButton_17.clicked.connect(self.choose17)
        self.pushButton_18.clicked.connect(self.choose18)

        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)

    def choose1(self):
        select = QMessageBox.question(self,'选择确认:','确定选择30级起步PVE模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.Level30PvE
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose2(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择50级起步PVE模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.Level50PvE
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose3(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择70级起步PVE模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.Level70PvE
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose4(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择0级起步标准PVE模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.StandardPvE
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose5(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择0级起步简单PVE模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.StandardPvE_Easy
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose6(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择0级起步困难PVE模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.StandardPvE_Hard
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose7(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择0级起步狂猎PVE模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.StandardPvE_Shuffle
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose8(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择30级起步PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.Level30PvP
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose9(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择50级起步PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.Level50PvP
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose10(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择70级起步PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.Level70PvP
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose11(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择0级起步标准PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.StandardPvP
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose12(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择0级起步简单PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.StandardPvP_Easy
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose13(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择0级起步困难PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.StandardPvP_Hard
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose14(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择0级起步狂猎PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.StandardPvP_Shuffle
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose15(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择单人SOLO模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.SoloPvP
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose16(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择双人PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.DuoPvP
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose17(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择单人PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.HardcorePvP
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def choose18(self):
        select = QMessageBox.question(self, '确认您的选择:', '确定选择双人PVP模式?')
        if select == QMessageBox.StandardButton.Yes:
            codes = offcial_codes.HardcoreDouPvP
            with open('ServerGameSettings.json', 'w',encoding='utf-8') as f:
                f.write(codes)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("官方配置选项")
        self.groupBox.setTitle("官配人机模式")
        self.pushButton.setText("30级起步人机模式")
        self.pushButton_2.setText("50级起步人机模式")
        self.pushButton_3.setText("70级起步人机模式")
        self.pushButton_4.setText("0级起步标准人机模式")
        self.pushButton_5.setText("0级起步简单人机模式")
        self.pushButton_6.setText("0级起步困难人机模式")
        self.pushButton_7.setText("0级起步狂猎人机模式")
        self.groupBox_2.setTitle("官配PVP模式")
        self.pushButton_8.setText("30级起步PVP模式")
        self.pushButton_9.setText("50级起步PVP模式")
        self.pushButton_10.setText("70级起步PVP模式")
        self.pushButton_11.setText("0级起步标准PVP模式")
        self.pushButton_12.setText("0级起步简单PVP模式")
        self.pushButton_13.setText("0级起步困难PVP模式")
        self.pushButton_14.setText("0级起步狂猎PVP模式")
        self.pushButton_15.setText("单人solo服")
        self.pushButton_16.setText("双人PVP服")
        self.pushButton_17.setText("单人PVP困难模式")
        self.pushButton_18.setText("双人PVP困难模式")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PyQt5设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PyQt5窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程
