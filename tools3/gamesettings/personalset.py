import sys
from PySide6.QtCore import Qt, QMetaObject, QRect
from PySide6.QtGui import QIcon, QPixmap, Qt, QFont
from PySide6.QtWidgets import QWidget, QTabWidget, QGroupBox, QLabel, QPushButton, QApplication, QMainWindow
from public.makelabel import *
from images.images import *
from public.makelabel import CombLabel, SpinLabel, DoubleLabel

f1 = QFont()
f1.setBold(True)
f1.setPointSize(12)


class Ui_MainWindow(QMainWindow):

    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置


    def setupUi(self, MainWindow):
        self.str1 = """
  "GameModeType": "PvP",
  "PlayerDamageMode": "Always",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "PvPProtectionMode": "Disabled",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "TeleportBoundItems": true,
  "AllowGlobalChat": true,
  "AllWaypointsUnlocked": true,
  "FreeCastleClaim": true,
  "FreeCastleDestroy": true,
  "InactivityKillEnabled": true,
  "InactivityKillTimeMin": 3600,
  "InactivityKillTimeMax": 604800,
  "InactivityKillSafeTimeAddition": 172800,
  "InactivityKillTimerMaxItemLevel": 84,
  "DisableDisconnectedDeadEnabled": true,
  "DisableDisconnectedDeadTimer": 60,
  "ClanSize": 4,
  "HolyAreaStrengthModifier": 1.0,
  "GarlicAreaStrengthModifier": 1.0,
  "SilverStrengthModifier": 1.0,
  "SunDamageModifier": 1.0,
  "StarterEquipmentId": "0",
  "StarterResourcesId": "0",
  "GameTimeModifiers": {
    "DayDurationInSeconds": 1080,
    "DayStartHour":  9,
    "DayStartMinute": 0,
    "DayEndHour":  17,
    "DayEndMinute": 0,
    "BloodMoonFrequency_Min": 10,
    "BloodMoonFrequency_Max": 18,
    "BloodMoonBuff": 0.2
  },
        """

        self.str2 = """ 
  "CastleDamageMode": "Never",
  "SiegeWeaponHealth": "VeryLow",
  "CastleSiegeTimer": 420,
  "CastleUnderAttackTimer": 60,
  "AnnounceSiegeWeaponSpawn": true,
  "ShowSiegeWeaponMapIcon": true,
  "PlayerInteractionSettings": {
    "TimeZone": "Local",
    "VSPlayerWeekdayTime": {
      "StartHour":  17,
      "StartMinute": 0,
      "EndHour":  23,
      "EndMinute": 0
    },
    "VSPlayerWeekendTime": {
      "StartHour":  17,
      "StartMinute": 0,
      "EndHour":  23,
      "EndMinute": 0
    },
    "VSCastleWeekdayTime": {
      "StartHour":  17,
      "StartMinute": 0,
      "EndHour":  23,
      "EndMinute": 0
    },
    "VSPCastleWeekendTime": {
      "StartHour":  17,
      "StartMinute": 0,
      "EndHour":  23,
      "EndMinute": 0
    }
  },
"""

        self.str3 = """
  "PvPVampireRespawnModifier": 1.0,
  "BloodDrainModifier": 1.0,
  "Death_DurabilityFactorLoss": 0.25,
  "Death_DurabilityLossFactorAsResources": 1.0,
  "VampireStatModifiers": {
    "MaxHealthModifier": 1.0,
    "MaxEnergyModifier": 1.0,
    "PhysicalPowerModifier": 1.0,
    "SpellPowerModifier": 1.0,
    "ResourcePowerModifier": 1.0,
    "SiegePowerModifier": 1.0,
    "DamageReceivedModifier": 1.0,
    "ReviveCancelDelay": 5.0
  },
  "DropTableModifier_General": 1.0,
  "DropTableModifier_Missions": 1.0,
"""

        self.str4 = """
  "DurabilityDrainModifier": 1.0,
  "EquipmentStatModifiers_Global": {
    "MaxHealthModifier": 1.0,
    "MaxEnergyModifier": 1.0,
    "ResourceYieldModifier": 1.0,
    "PhysicalPowerModifier": 1.0,
    "SpellPowerModifier": 1.0,
    "SiegePowerModifier": 1.0,
    "MovementSpeedModifier": 1.0
  },
                    """

        self.str5 = """
  "InventoryStacksModifier": 1.0,
  "MaterialYieldModifier_Global": 1.0,
  "BloodEssenceYieldModifier": 1.0,
  "BuildCostModifier": 1.0,
  "RecipeCostModifier": 1.0,
  "CraftRateModifier": 1.0,
  "ResearchCostModifier": 1.0,
  "ResearchTimeModifier": 1.0,
  "RefinementCostModifier": 1.0,
  "RefinementRateModifier": 1.0,
  "DismantleResourceModifier": 0.75,
  "ServantConvertRateModifier": 1.0,
  "RepairCostModifier": 1.0,
        """

        self.str6 ="""
  "UnitStatModifiers_Global": {
    "MaxHealthModifier": 1.0,
    "PowerModifier": 1.0
  },
  "UnitStatModifiers_VBlood": {
    "MaxHealthModifier": 1.0,
    "PowerModifier": 1.0
  },
"""
        self.str7 = """
  "UnlockedResearchs": [],
  "UnlockedAchievements": [],
  "VBloodUnitSettings": [
    {
      "UnitId": -1905691330,
      "UnitLevel": 16,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 1124739990,
      "UnitLevel": 20,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 2122229952,
      "UnitLevel": 20,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -2025101517,
      "UnitLevel": 20,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 763273073,
      "UnitLevel": 26,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 1106149033,
      "UnitLevel": 27,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 577478542,
      "UnitLevel": 27,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -2039908510,
      "UnitLevel": 30,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 1896428751,
      "UnitLevel": 30,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -484556888,
      "UnitLevel": 34,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1391546313,
      "UnitLevel": 36,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 153390636,
      "UnitLevel": 37,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1659822956,
      "UnitLevel": 37,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1942352521,
      "UnitLevel": 38,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -29797003,
      "UnitLevel": 40,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -99012450,
      "UnitLevel": 44,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 939467639,
      "UnitLevel": 46,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1449631170,
      "UnitLevel": 46,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1065970933,
      "UnitLevel": 48,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 850622034,
      "UnitLevel": 52,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 24378719,
      "UnitLevel": 56,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1688478381,
      "UnitLevel": 58,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -680831417,
      "UnitLevel": 60,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -548489519,
      "UnitLevel": 60,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -203043163,
      "UnitLevel": 62,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1208888966,
      "UnitLevel": 62,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1968372384,
      "UnitLevel": 62,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 1007062401,
      "UnitLevel": 64,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -2013903325,
      "UnitLevel": 64,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 685266977,
      "UnitLevel": 68,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 114912615,
      "UnitLevel": 68,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1347412392,
      "UnitLevel": 68,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -910296704,
      "UnitLevel": 72,
      "DefaultUnlocked": false
    },
    {
      "UnitId": 1112948824,
      "UnitLevel": 76,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -1936575244,
      "UnitLevel": 78,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -393555055,
      "UnitLevel": 78,
      "DefaultUnlocked": false
    },
    {
      "UnitId": -740796338,
      "UnitLevel": 80,
      "DefaultUnlocked": false
    }
 ]
        """

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setWindowFlag(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        # self.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        icon = QIcon()
        icon.addPixmap(QPixmap(":/set/set.jpg"), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("central-widget")
        self.tabWidget = QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QRect(0, 0, 800, 600))
        font = QFont()
        font.setFamily("微软雅黑")
        font.setPointSize(10)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")

        self.tab_1 = QWidget()
        self.tab_1.setObjectName("tab_1")
        self.groupBox = QGroupBox(self.tab_1)
        self.groupBox.setAutoFillBackground(True)
        self.groupBox.setGeometry(QRect(30, 20, 740, 540))
        self.groupBox.setAlignment(Qt.AlignCenter)
        self.groupBox.setFlat(False)
        self.groupBox.setCheckable(False)
        self.groupBox.setChecked(False)
        self.groupBox.setFont(f1)
        self.groupBox.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(255, 0, 0);")
        self.groupBox.setObjectName("groupBox")
        # 加载自定义标签

        self.comblabel1 = CombLabel(self.groupBox, 30, 20, "GameModeType", "游戏模式选择:", ['PvP', 'PvE'],
                                    ['玩家对战', '人机模式'])
        self.comblabel1.comboBox.activated.connect(self.enble)
        self.comblabel2 = CombLabel(self.groupBox, 30, 50, "PlayerDamageMode", "玩家伤害模式",
                                    ['Always', 'TimeRestricted'],
                                    ['全天可PK', '限时PK'])
        self.comblabel3 = CombLabel(self.groupBox, 30, 80, 'CastleHeartDamageMode', '城堡之心伤害模式:',
                                    ['CanBeDestroyedOnlyWhenDecaying', 'CanBeDestroyedByPlayers',
                                     'CanBeSeizedOrDestroyedByPlayers'], ['无血液时', '只能被毁', '也能被偷'])
        self.comblabel4 = CombLabel(self.groupBox, 30, 110, "PvPProtectionMode", "新手保护时间:",
                                    ["Disabled", "VeryShort", "Short", "Medium", "Long"],
                                    ["无  0分钟", "很短  15分钟", "短  30分钟", "一般  60分钟", "长  120分钟"])
        self.comblabel5 = CombLabel(self.groupBox, 30, 140, 'DeathContainerPermission', '拾取死亡掉落',
                                    ['Anyone', 'ClanMembers', 'OnlySelf'], ['任何人', '仅部落成员', '仅死者'])
        self.comblabel6 = CombLabel(self.groupBox, 30, 170, 'RelicSpawnType', '灵魂碎片数量', ['Unique', 'Plentiful'],
                                    ['唯一', '不限'])
        self.comblabel7 = CombLabel(self.groupBox, 30, 200, 'BloodBoundEquipment', '装备绑定(免爆)', ['true', 'false'],
                                    ['不爆', '可爆'])
        self.comblabel8 = CombLabel(self.groupBox, 30, 230, "TeleportBoundItems", "禁止带物传送:", ['true', 'false'],
                                    ["开启", "关闭"])
        self.comblabel9 = CombLabel(self.groupBox, 30, 260, "AllowGlobalChat", "全服聊天功能:", ['true', 'false'],
                                    ["开启", "关闭"])
        self.comblabel10 = CombLabel(self.groupBox, 30, 290, "AllWaypointsUnlocked", "解锁地图传送点:",
                                     ['true', 'false'],
                                     ["解锁", "不解锁"])
        self.comblabel11 = CombLabel(self.groupBox, 30, 320, "FreeCastleClaim", "城堡能否被偷取", ['true', 'false'],
                                     ["直接偷取", "需要钥匙"])
        self.comblabel12 = CombLabel(self.groupBox, 30, 350, "FreeCastleDestroy", "直接摧毁敌方城堡:",
                                     ['true', 'false'],
                                     ["可以摧毁", "需要钥匙"])
        self.comblabel13 = CombLabel(self.groupBox, 30, 380, "InactivityKillEnabled", "非活动清理:", ['true', 'false'],
                                     ["开启", "不开启"])
        self.comblabel14 = SpinLabel(self.groupBox, 30, 410, "InactivityKillTimeMin", "清理最小时间:", 3600, 0, 3600,
                                     60)
        self.comblabel15 = SpinLabel(self.groupBox, 30, 440, "InactivityKillTimeMax", "清理最大时间:", 604800, 0,
                                     999999, 600)
        self.comblabel16 = SpinLabel(self.groupBox, 30, 470, "InactivityKillSafeTimeAddition", '增加清理保护时间:',
                                     172800, 0,
                                     999999, 3600)
        self.comblabel17 = SpinLabel(self.groupBox, 400, 20, "InactivityKillTimerMaxItemLevel", "清理对象的等级限制:",
                                     84, 0, 99, 1)
        self.comblabel18 = CombLabel(self.groupBox, 400, 50, "DisableDisconnectedDeadEnabled", "断开无效连接:",
                                     ['true', 'false'], ["断开", "不断开"])
        self.comblabel19 = SpinLabel(self.groupBox, 400, 80, "DisableDisconnectedDeadTimer", "无效连接断开(分钟):",
                                     60, 0, 9999, 10)
        self.comblabel20 = SpinLabel(self.groupBox, 400, 110, "ClanSize", "部落(氏族)人数上限:", 4, 1, 10, 1)
        self.comblabel21 = DoubleLabel(self.groupBox, 400, 140, "HolyAreaStrengthModifier", "神圣DeBUFF强度:",
                                       1.0, 0, 5, 0.1, 1)
        self.comblabel22 = DoubleLabel(self.groupBox, 400, 170, "GarlicAreaStrengthModifier", "大蒜DeBUFF强度:",
                                       1.0, 0, 5, 0.1, 1)
        self.comblabel23 = DoubleLabel(self.groupBox, 400, 200, "SilverStrengthModifier", "银器DeBUFF强度:",
                                       1.0, 0, 5, 0.1, 1)
        self.comblabel24 = DoubleLabel(self.groupBox, 400, 230, "SunDamageModifier", "阳光伤害强度:", 1.0, 0, 5, 0.1, 1)
        self.comblabel25 = CombLabel(self.groupBox, 400, 260, "StarterEquipmentId", "新建角色装备等级:",
                                     ["0", "-376135143", "-1613823352", "-258598606", "1177675846"],
                                     ["0级", "30级", "50级", "70级", "80级"])
        self.comblabel26 = CombLabel(self.groupBox, 400, 290, "StarterResourcesId", "新建角色启动资源:",
                                     ["0", "-696202180", '481718792', "-766909665"], ["0级", "30级", "50级", "70级"])
        self.comblabel27 = SpinLabel(self.groupBox, 400, 320, "DayDurationInSeconds", "白天时间长度:", 1080, 60, 86400,
                                     300)
        self.comblabel28 = TimeLabel(self.groupBox, 400, 350, "DayStartTime", "白天开始时间:", (9, 0, 0))
        self.comblabel29 = TimeLabel(self.groupBox, 400, 380, "DayEndTime", "白天结束时间:", (17, 0, 0))
        self.comblabel30 = SpinLabel(self.groupBox, 400, 410, "BloodMoonFrequency_Min", "血月出现的最小频率:", 10, 1,
                                     255, 1)
        self.comblabel31 = SpinLabel(self.groupBox, 400, 440, "BloodMoonFrequency_Max", "血月出现的最大频率:", 18, 1,
                                     255, 1)
        self.comblabel32 = DoubleLabel(self.groupBox, 400, 470, "BloodMoonBuff", "血月增益:", 0.2, 0.1, 1, 0.1, 1)
        self.pushButton1 = QPushButton(self.groupBox)
        self.pushButton1.setGeometry(QRect(325, 500, 100, 35))
        font = QFont()
        font.setPointSize(10)
        self.pushButton1.setFont(font)
        self.pushButton1.setObjectName("pushButton1")
        self.pushButton1.setText('保存以上设置')
        self.pushButton1.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(0, 0, 255);")
        self.pushButton1.clicked.connect(self.savedata)
        self.tabWidget.addTab(self.tab_1, "")

        self.tab_2 = QWidget()
        self.tab_2.setObjectName("tab_2")
        self.groupBox2 = QGroupBox(self.tab_2)
        self.groupBox2.setAutoFillBackground(True)
        self.groupBox2.setGeometry(QRect(200, 20, 360, 540))
        self.groupBox2.setAlignment(Qt.AlignCenter)
        self.groupBox2.setFlat(False)
        self.groupBox2.setCheckable(False)
        self.groupBox2.setChecked(False)
        self.groupBox2.setFont(f1)
        self.groupBox2.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(255, 0, 0);")
        self.groupBox2.setObjectName("groupBox2")

        self.comblabel33 = CombLabel(self.groupBox2, 30, 40, "CastleDamageMode", "城堡伤害模式:",
                                     ["Never", "Always", "TimeRestricted"],
                                     ["不可以攻城", "随时攻城", "指定时间内攻城"])
        self.comblabel34 = CombLabel(self.groupBox2, 30, 70, "SiegeWeaponHealth", "攻城傀儡生命:",
                                     ["VeryLow", "Low", "Normal", "High", "VeryHigh"],
                                     ["最低500HP", "低1000HP", "正常1500HP", "高2000HP", "最高2500HP"])
        self.comblabel35 = SpinLabel(self.groupBox2, 30, 100, "CastleSiegeTimer", "破门后倒计时(秒):", 420, 1, 1800, 60)
        self.comblabel36 = SpinLabel(self.groupBox2, 30, 130, "CastleUnderAttackTimer", "守城被阻止建造(秒):",
                                     60, 1, 180, 10)
        self.comblabel37 = CombLabel(self.groupBox2, 30, 160, "AnnounceSiegeWeaponSpawn", "攻城傀儡生成通告:",
                                     ['true', 'false'], ["开启", "关闭"])
        self.comblabel38 = CombLabel(self.groupBox2, 30, 190, "ShowSiegeWeaponMapIcon", "攻城傀儡地图标记:",
                                     ['true', 'false'], ["开启", "关闭"])
        self.comblabel39 = CombLabel(self.groupBox2, 30, 220, "TimeZone", "时区设置:",
                                     ["Local", "UTC", "PST", "CET", "CST"],
                                     ["服务器本地时间", "太平洋时间", "美国东部时间", "中欧时间", "中国标准时间"])
        self.comblabel40 = TimeLabel(self.groupBox2, 30, 250, "StartTime", "工作日PK开始时间:", (17, 0, 0))
        self.comblabel41 = TimeLabel(self.groupBox2, 30, 280, "EndTime", "工作日PK结束时间:", (23, 0, 0))
        self.comblabel42 = TimeLabel(self.groupBox2, 30, 310, "StartTime", "周末PK开始时间:", (17, 0, 0))
        self.comblabel43 = TimeLabel(self.groupBox2, 30, 340, "EndTime", "周末PK结束时间:", (23, 0, 0))
        self.comblabel44 = TimeLabel(self.groupBox2, 30, 370, "StartTime", "工作日攻城开始时间:", (17, 0, 0))
        self.comblabel45 = TimeLabel(self.groupBox2, 30, 400, "EndTime", "工作日攻城结束时间:", (23, 0, 0))
        self.comblabel46 = TimeLabel(self.groupBox2, 30, 430, "StartTime", "周末攻城开始时间:", (17, 0, 0))
        self.comblabel47 = TimeLabel(self.groupBox2, 30, 460, "EndTime", "周末攻城结束时间:", (23, 0, 0))
        self.pushButton2 = QPushButton(self.groupBox2)
        self.pushButton2.setGeometry(QRect(130, 500, 100, 35))
        font = QFont()
        font.setPointSize(10)
        self.pushButton2.setFont(font)
        self.pushButton2.setObjectName("pushButton2")
        self.pushButton2.setText('保存以上设置')
        self.pushButton2.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(0, 0, 255);")
        self.pushButton2.clicked.connect(self.savedata2)
        self.tabWidget.addTab(self.tab_2, "")

        self.tab_3 = QWidget()
        self.tab_3.setObjectName("tab_3")
        self.groupBox3 = QGroupBox(self.tab_3)
        self.groupBox3.setAutoFillBackground(True)
        self.groupBox3.setGeometry(QRect(200, 20, 360, 540))
        self.groupBox3.setAlignment(Qt.AlignCenter)
        self.groupBox3.setFlat(False)
        self.groupBox3.setCheckable(False)
        self.groupBox3.setChecked(False)
        self.groupBox3.setFont(f1)
        self.groupBox3.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(255, 0, 0);")
        self.groupBox3.setObjectName("groupBox3")
        self.comblabel48 = DoubleLabel(self.groupBox3, 30, 50, "PvPVampireRespawnModifier", "PK复活加延迟(正比):", 1, 1,
                                       5, 0.1, 1)
        self.comblabel49 = DoubleLabel(self.groupBox3, 30, 80, "BloodDrainModifier", "角色血液损失率:", 1, 1, 5, 0.1, 1)
        self.comblabel50 = DoubleLabel(self.groupBox3, 30, 110, "Death_DurabilityFactorLoss", "角色死亡掉持久比例:",
                                       0.25, 0, 1, 0.1, 2)
        self.comblabel51 = DoubleLabel(self.groupBox3, 30, 140, "Death_DurabilityLossFactorAsResources",
                                       "死亡持久转材料爆率:",
                                       1, 0, 1, 0.1, 1)
        self.comblabel52 = DoubleLabel(self.groupBox3, 30, 170, "MaxHealthModifier", "角色生命倍数:", 1, 0.1, 10, 0.5,
                                       1)
        self.comblabel53 = DoubleLabel(self.groupBox3, 30, 200, "MaxEnergyModifier", "角色能量倍数:", 1, 0.1, 10, 0.5,
                                       1)
        self.comblabel54 = DoubleLabel(self.groupBox3, 30, 230, "PhysicalPowerModifier", "攻击力倍数:", 1, 0.1, 10, 0.5,
                                       1)
        self.comblabel55 = DoubleLabel(self.groupBox3, 30, 260, "SpellPowerModifier", "法术力倍数:", 1, 0.1, 10, 0.5, 1)
        self.comblabel56 = DoubleLabel(self.groupBox3, 30, 290, "ResourcePowerModifier", "对资源的伤害倍数:", 1, 0.1,
                                       10, 0.5, 1)
        self.comblabel57 = DoubleLabel(self.groupBox3, 30, 320, "SiegePowerModifier", "对城堡的伤害倍数:", 1, 0.1, 10,
                                       0.5, 1)
        self.comblabel58 = DoubleLabel(self.groupBox3, 30, 350, "DamageReceivedModifier", "角色受到伤害的倍数:", 1, 0.1,
                                       10, 0.5, 1)
        self.comblabel59 = DoubleLabel(self.groupBox3, 30, 380, "ReviveCancelDelay", "复活延迟:", 5, 0, 9999, 1, 1)
        self.comblabel60 = DoubleLabel(self.groupBox3, 30, 410, "DropTableModifier_General", "战利品掉落倍数:", 1, 0.25,
                                       5, 0.25, 2)
        self.comblabel61 = DoubleLabel(self.groupBox3, 30, 440, "DropTableModifier_Missions", "仆人打工战利员倍数:",
                                       1, 0.25, 5, 0.25, 2)
        self.pushButton3 = QPushButton(self.groupBox3)
        self.pushButton3.setGeometry(QRect(130, 500, 100, 35))
        font = QFont()
        font.setPointSize(10)
        self.pushButton3.setFont(font)
        self.pushButton3.setObjectName("pushButton3")
        self.pushButton3.setText('保存以上设置')
        self.pushButton3.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(0, 0, 255);")
        self.pushButton3.clicked.connect(self.savedata3)
        self.tabWidget.addTab(self.tab_3, "")

        self.tab_4 = QWidget()
        self.tab_4.setObjectName("tab_4")
        self.groupBox4 = QGroupBox(self.tab_4)
        self.groupBox4.setAutoFillBackground(True)
        self.groupBox4.setGeometry(QRect(30, 20, 340, 360))
        self.groupBox4.setAlignment(Qt.AlignCenter)
        self.groupBox4.setFlat(False)
        self.groupBox4.setCheckable(False)
        self.groupBox4.setChecked(False)
        self.groupBox4.setFont(f1)
        self.groupBox4.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(255, 0, 0);")
        self.groupBox4.setObjectName("groupBox4")
        self.comblabel62 = DoubleLabel(self.groupBox4, 30, 50, "DurabilityDrainModifier", "装备持久损失率:",
                                       1, 0, 5, 0.5, 1)
        self.comblabel63 = DoubleLabel(self.groupBox4, 30, 80, "MaxHealthModifier", "装备最大生命倍数:",
                                       1, 0.1, 10, 0.5, 1)
        self.comblabel64 = DoubleLabel(self.groupBox4, 30, 110, "MaxEnergyModifier", "装备最大能量倍数:",
                                       1, 0.1, 10, 0.5, 1)
        self.comblabel65 = DoubleLabel(self.groupBox4, 30, 140, "ResourceYieldModifier", "装备资源产量倍数:",
                                       1, 0.1, 10, 0.5, 1)
        self.comblabel66 = DoubleLabel(self.groupBox4, 30, 170, "PhysicalPowerModifier", "装备物理攻击倍数:",
                                       1, 0.1, 10, 0.5, 1)
        self.comblabel67 = DoubleLabel(self.groupBox4, 30, 200, "SpellPowerModifier", "装备法术攻击倍数:",
                                       1, 0.1, 10, 0.5, 1)
        self.comblabel68 = DoubleLabel(self.groupBox4, 30, 230, "SiegePowerModifier", "对城堡的伤害倍数:",
                                       1, 0.1, 10, 0.5, 1)
        self.comblabel69 = DoubleLabel(self.groupBox4, 30, 260, "MovementSpeedModifier", "装备移速倍数:", 1, 0.1, 10,
                                       0.5, 1)

        self.pushButton4 = QPushButton(self.groupBox4)
        self.pushButton4.setGeometry(QRect(119, 310, 100, 35))
        font = QFont()
        font.setPointSize(10)
        self.pushButton4.setFont(font)
        self.pushButton4.setObjectName("pushButton3")
        self.pushButton4.setText('保存装备设置')
        self.pushButton4.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(0, 0, 255);")
        self.pushButton4.clicked.connect(self.savedata4)

        self.groupBox5 = QGroupBox(self.tab_4)
        self.groupBox5.setAutoFillBackground(True)
        self.groupBox5.setGeometry(QRect(430, 90, 340, 470))
        self.groupBox5.setAlignment(Qt.AlignCenter)
        self.groupBox5.setFlat(False)
        self.groupBox5.setCheckable(False)
        self.groupBox5.setChecked(False)
        self.groupBox5.setFont(f1)
        self.groupBox5.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(255, 0, 0);")
        self.groupBox5.setObjectName("groupBox5")
        self.comblabel70 = DoubleLabel(self.groupBox5, 30, 40, "InventoryStacksModifier", "物品叠加倍数:",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel71 = DoubleLabel(self.groupBox5, 30, 70, "MaterialYieldModifier_Global", "资源材料产量(全服):",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel72 = DoubleLabel(self.groupBox5, 30, 100, "BloodEssenceYieldModifier", "血液精华产量(全服):",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel73 = DoubleLabel(self.groupBox5, 30, 130, "BuildCostModifier", "城堡建造成本比例:",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel74 = DoubleLabel(self.groupBox5, 30, 160, "RecipeCostModifier", "生产配方成本比例:",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel75 = DoubleLabel(self.groupBox5, 30, 190, "CraftRateModifier", "生产制造物品速率:",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel76 = DoubleLabel(self.groupBox5, 30, 220, "ResearchCostModifier", "研究成本消耗比例:",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel77 = DoubleLabel(self.groupBox5, 30, 250, "ResearchTimeModifier", "研究时间比例:",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel78 = DoubleLabel(self.groupBox5, 30, 280, "RefinementCostModifier", "精炼成本消耗比例:",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel79 = DoubleLabel(self.groupBox5, 30, 310, "RefinementRateModifier", "精炼速度:",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel80 = DoubleLabel(self.groupBox5, 30, 340, "DismantleResourceModifier", "拆除返还比例:",
                                       0.75, 0, 1, 0.25, 2)
        self.comblabel81 = DoubleLabel(self.groupBox5, 30, 370, "ServantConvertRateModifier", "仆人转化速度:",
                                       1, 0.1, 99, 0.5, 1)
        self.comblabel82 = DoubleLabel(self.groupBox5, 30, 400, "RepairCostModifier", "维修消耗比例:",
                                       1, 0, 99, 0.5, 1)
        self.pushButton5 = QPushButton(self.groupBox5)
        self.pushButton5.setGeometry(QRect(130, 430, 100, 35))
        font = QFont()
        font.setPointSize(10)
        self.pushButton5.setFont(font)
        self.pushButton5.setObjectName("pushButton5")
        self.pushButton5.setText('保存装备设置')
        self.pushButton5.clicked.connect(self.savedata5)
        self.pushButton5.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(0, 0, 255);")
        self.tabWidget.addTab(self.tab_4, "")

        self.tab_5 = QWidget()
        self.tab_5.setObjectName("tab_5")
        self.groupBox7 = QGroupBox(self.tab_5)
        self.groupBox7.setAutoFillBackground(True)
        self.groupBox7.setGeometry(QRect(10, 10, 780, 550))
        self.groupBox7.setAlignment(Qt.AlignLeft)
        self.groupBox7.setFlat(False)
        self.groupBox7.setCheckable(False)
        self.groupBox7.setChecked(False)
        self.groupBox7.setFont(f1)
        self.groupBox7.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(255, 0, 0);")
        self.groupBox7.setObjectName("groupBox6")
        self.comblabel87 = CombLabel(self.groupBox7, 30, 30, "UnlockedResearchs", '开服解锁研究等级:',
                                     ["[]", "[\n -495424062\n ]", "[\n -495424062,\n -1292809886\n ]",
                                      "[\n -495424062,\n -1292809886,\n -1262194203]"],
                                     ["不解锁", "解锁至30级", "解锁至50级", "全部解锁至70级"])
        l1 = "[\n -1770927128,\n 436375429,\n -1400391027,\n -2071097880,\n 1695239324,\n 1502386974,\n 1694767961,\n -1899098914,\n 560247139,\n -1995132640,\n -1434604634\n ]"
        l2 = "[\n -1770927128,\n 436375429,\n -1400391027,\n -2071097880,\n 1695239324,\n 1502386974,\n 1694767961,\n -1899098914,\n 560247139,\n -1995132640,\n -1434604634,\n 1668809517,\n 334973636,\n 606418711,\n -892747762,\n -437605270\n ]"
        l3 = "[\n -1770927128,\n 436375429,\n -1400391027,\n -2071097880,\n 1695239324,\n 1502386974,\n 1694767961,\n -1899098914,\n 560247139,\n -1995132640,\n -1434604634,\n 1668809517,\n 334973636,\n 606418711,\n -892747762,\n -437605270,\n -2104585843,\n -327597689\n ]"
        self.comblabel88 = CombLabel(self.groupBox7, 400, 30, "UnlockedAchievements", '开服游戏任务免做设置:',
                                     ["[]", l1, l2, l3],
                                     ["不跳过任何任务进度", "只开启30级以上进度", "只开启50级以上进度",
                                      "只开启70级以上进度"])

        self.label_notice1 = QLabel(self.groupBox7)
        self.label_notice1.setGeometry(QRect(5, 60, 740, 25))
        self.label_notice1.setObjectName('VBloodUnitSettings')
        self.label_notice1.setText(
            'VBoss解锁设置说明:如果解锁了相应的Boss,玩家进入游戏后,Boss所对应的击杀成就和技能将会解锁.')
        ff = QFont()
        ff.setBold(True)
        ff.setPointSize(11)
        self.label_notice1.setFont(ff)
        self.label_notice2 = QLabel(self.groupBox7)
        self.label_notice2.setGeometry(QRect(5, 90, 740, 25))
        self.label_notice2.setObjectName('VBloodUnitSettings')
        self.label_notice2.setText('修改VBoss等级会给玩家带来游戏进程的推进或延迟,开服新手修改慎重!!!')
        ff = QFont()
        ff.setBold(True)
        ff.setPointSize(11)
        self.label_notice2.setFont(ff)
        self.checklabel1 = CheckLabel(self.groupBox7, '-1905691330', 0, 120, '阿尔法狼:', '16')
        self.checklabel2 = CheckLabel(self.groupBox7, '1124739990', 0, 150, '冰霜弓箭手基利:', '20')
        self.checklabel3 = CheckLabel(self.groupBox7, '2122229952', 0, 180, '工头鲁弗斯:', '20')
        self.checklabel4 = CheckLabel(self.groupBox7, '-2025101517', 0, 210, '碎石斗士埃罗尔:', '20')
        self.checklabel5 = CheckLabel(self.groupBox7, '763273073', 0, 240, '混沌射手莉迪亚:', '26')
        self.checklabel6 = CheckLabel(self.groupBox7, '1106149033', 0, 270, '毁灭者戈雷斯温:', '27')
        self.checklabel7 = CheckLabel(self.groupBox7, '577478542', 0, 300, '甲胄师格雷森:', '27')
        self.checklabel8 = CheckLabel(self.groupBox7, '-2039908510', 0, 330, '腐鼠:', '30')
        self.checklabel9 = CheckLabel(self.groupBox7, '1896428751', 0, 360, '纵火狂克里夫:', '30')
        self.checklabel10 = CheckLabel(self.groupBox7, '-484556888', 0, 390, '精灵游侠波洛拉:', '34')
        self.checklabel11 = CheckLabel(self.groupBox7, '-1391546313', 0, 420, '恶熊:', '36')
        self.checklabel12 = CheckLabel(self.groupBox7, '153390636', 0, 450, '堕落者尼古拉斯:', '37')
        self.checklabel13 = CheckLabel(self.groupBox7, "-1659822956", 0, 480, '强盗之王昆西:', '37')
        self.checklabel14 = CheckLabel(self.groupBox7, "-1942352521", 260, 120, '裁缝比阿特丽斯:', '38')
        self.checklabel15 = CheckLabel(self.groupBox7, "-29797003", 260, 150, '冰霜使者文森特:', '40')
        self.checklabel16 = CheckLabel(self.groupBox7, "-99012450", 260, 180, '祭司克里斯蒂娜:', '44')
        self.checklabel17 = CheckLabel(self.groupBox7, '939467639', 260, 210, '猎人特里斯坦:', '46')
        self.checklabel18 = CheckLabel(self.groupBox7, "-1449631170", 260, 240, '暗影祭司琳德拉:', '46')
        # self.checklabel19 = CheckLabel(self.groupBox7, '-1905691330', 260,270, '地卜师泰拉:', '44')
        self.checklabel19 = CheckLabel(self.groupBox7, '-1065970933', 260, 270, '地卜师泰拉:', '48')
        self.checklabel20 = CheckLabel(self.groupBox7, '850622034', 260, 300, '弓箭手梅雷迪斯:', '52')
        self.checklabel21 = CheckLabel(self.groupBox7, '24378719', 260, 330, '雪人骇兽霜喉:', '56')
        self.checklabel22 = CheckLabel(self.groupBox7, '-1688478381', 260, 360, '民兵队长屋大维:', '58')
        self.checklabel23 = CheckLabel(self.groupBox7, "-680831417", 260, 390, '牧羊人拉齐尔:', '60')
        self.checklabel24 = CheckLabel(self.groupBox7, "-548489519", 260, 420, '蜘蛛女王乌戈拉:', '60')
        self.checklabel25 = CheckLabel(self.groupBox7, "-203043163", 260, 450, '巴拉顿公爵:', '62')
        self.checklabel26 = CheckLabel(self.groupBox7, "-1208888966", 260, 480, '吸血鬼猎人贾德:', '62')
        self.checklabel27 = CheckLabel(self.groupBox7, "-1968372384", 525, 120, '夺魂者福洛特:', '62')
        self.checklabel28 = CheckLabel(self.groupBox7, "1007062401", 525, 150, '狼人首领:', '64')
        self.checklabel29 = CheckLabel(self.groupBox7, '-2013903325', 525, 180, '元素法师梅尔文:', '64')
        self.checklabel30 = CheckLabel(self.groupBox7, "685266977", 525, 210, '太阳使者:', '68')
        self.checklabel31 = CheckLabel(self.groupBox7, '114912615', 525, 240, '食人魔骇爪:', '68')
        self.checklabel32 = CheckLabel(self.groupBox7, "-1347412392", 525, 270, '风翼族长莫里安:', '68')
        self.checklabel33 = CheckLabel(self.groupBox7, "-910296704", 525, 300, '编咒师玛特卡:', '72')
        self.checklabel34 = CheckLabel(self.groupBox7, '1112948824', 525, 330, '暗夜元帅冥河:', '76')
        self.checklabel35 = CheckLabel(self.groupBox7, "-1936575244", 525, 360, '巨兽戈雷库什:', '78')
        self.checklabel36 = CheckLabel(self.groupBox7, "-393555055", 525, 390, '恐怖翼兽:', '78')
        self.checklabel37 = CheckLabel(self.groupBox7, "-740796338", 525, 420, '无瑕者索拉鲁斯:', '80')
        self.pushButton7 = QPushButton(self.groupBox7)
        self.pushButton7.setGeometry(QRect(560, 485, 120, 35))
        font = QFont()
        font.setPointSize(11)
        font.setBold(True)
        self.pushButton7.setFont(font)
        self.pushButton7.setObjectName("pushButton7")
        self.pushButton7.setText('保存BOSS设置')
        self.pushButton7.clicked.connect(self.savedata7)
        self.pushButton7.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(0, 0, 255);")
        self.tabWidget.addTab(self.tab_5, '')

        self.tab_6 = QWidget()
        self.tab_6.setObjectName("tab_7")
        self.groupBox6 = QGroupBox(self.tab_6)
        self.groupBox6.setAutoFillBackground(True)
        self.groupBox6.setGeometry(QRect(30, 170, 740, 130))
        self.groupBox6.setAlignment(Qt.AlignCenter)
        self.groupBox6.setFlat(False)
        self.groupBox6.setCheckable(False)
        self.groupBox6.setChecked(False)
        self.groupBox6.setFont(f1)
        self.groupBox6.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(255, 0, 0);")
        self.groupBox6.setObjectName("groupBox6")
        self.comblabel83 = DoubleLabel(self.groupBox6, 30, 30, "MaxHealthModifier", "AI单位生命倍数:",
                                       1, 0.1, 5, 0.1, 1)
        self.comblabel84 = DoubleLabel(self.groupBox6, 400, 30, "PowerModifier", "AI单位伤害倍数:",
                                       1, 0.1, 5, 0.1, 1)
        self.comblabel85 = DoubleLabel(self.groupBox6, 30, 60, "MaxHealthModifier", "V血BOSS的生命倍数:",
                                       1, 0.1, 5, 0.1, 1)
        self.comblabel86 = DoubleLabel(self.groupBox6, 400, 60, "PowerModifier", "V血BOSS的伤害倍数:",
                                       1, 0.1, 5, 0.1, 1)
        self.pushButton6 = QPushButton(self.groupBox6)
        self.pushButton6.setGeometry(QRect(325, 90, 100, 35))
        font = QFont()
        font.setPointSize(10)
        self.pushButton6.setFont(font)
        self.pushButton6.setObjectName("pushButton5")
        self.pushButton6.setText('保存怪物设置')
        self.pushButton6.clicked.connect(self.savedata6)
        self.pushButton6.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(0, 0, 255);")
        self.tabWidget.addTab(self.tab_6, "")

        MainWindow.setCentralWidget(self.centralwidget)
        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QMetaObject.connectSlotsByName(MainWindow)

    def savedata7(self):
        self.str7 = '  '+self.comblabel87.result() + ',\n  ' \
                    + self.comblabel88.result() \
                    + ',\n  "VBloodUnitSettings": [\n' + self.checklabel1.result() \
                    + self.checklabel2.result() + self.checklabel3.result() + self.checklabel4.result() \
                    + self.checklabel5.result() + self.checklabel6.result() + self.checklabel7.result() \
                    + self.checklabel8.result() + self.checklabel9.result() + self.checklabel10.result() \
                    + self.checklabel11.result() + self.checklabel12.result() + self.checklabel13.result() \
                    + self.checklabel14.result() + self.checklabel15.result() + self.checklabel16.result() \
                    + self.checklabel17.result() + self.checklabel18.result() + self.checklabel19.result() \
                    + self.checklabel20.result() + self.checklabel21.result() + self.checklabel22.result() \
                    + self.checklabel23.result() + self.checklabel24.result() + self.checklabel25.result() \
                    + self.checklabel26.result() + self.checklabel27.result() + self.checklabel28.result() \
                    + self.checklabel29.result() + self.checklabel30.result() + self.checklabel31.result() \
                    + self.checklabel32.result() + self.checklabel33.result() + self.checklabel34.result() \
                    + self.checklabel35.result() + self.checklabel36.result() + self.checklabel37.result()[:-3] \
                    + '}\n ]'
        with open('ServerGameSettings.json', 'w') as sgs:
            # self.str_total = self.str6 + self.str7
            self.str_total = '{\n'+self.str1 + self.str2 + self.str3 + self.str4 + self.str5 + self.str6+self.str7+'\n}'
            sgs.write(self.str_total)

    def savedata(self):
        self.str1 = '  ' + self.comblabel1.result() + ',\n  ' + self.comblabel2.result() + ',\n  ' \
                    + self.comblabel3.result() + ',\n  ' + self.comblabel4.result() + ',\n  ' + self.comblabel5.result() + ',\n  ' \
                    + self.comblabel6.result() + ',\n  ' + self.comblabel7.result() + ',\n  ' + self.comblabel8.result() + ',\n  ' \
                    + self.comblabel9.result() + ',\n  ' + self.comblabel10.result() + ',\n  ' + self.comblabel11.result() + ',\n  ' \
                    + self.comblabel12.result() + ',\n  ' + self.comblabel13.result() + ',\n  ' + self.comblabel14.result() + ',\n  ' \
                    + self.comblabel15.result() + ',\n  ' + self.comblabel16.result() + ',\n  ' + self.comblabel17.result() + ',\n  ' \
                    + self.comblabel18.result() + ',\n  ' + self.comblabel19.result() + ',\n  ' + self.comblabel20.result() + ',\n  ' \
                    + self.comblabel21.result() + ',\n  ' + self.comblabel22.result() + ',\n  ' + self.comblabel23.result() + ',\n  ' \
                    + self.comblabel24.result() + ',\n  ' + self.comblabel25.result() + ',\n  ' + self.comblabel26.result() + ',\n  ' \
                    '"GameTimeModifiers": {\n    ' + self.comblabel27.result() + ',\n    ' \
                    + '"DayStartHour": ' + self.comblabel28.result().split(':')[1] + ',\n    ' \
                    + '"DayStartMinute": ' + self.comblabel28.result().split(':')[2] + ',\n    ' \
                    + '"DayEndHour": ' + self.comblabel29.result().split(':')[1] + ',\n    ' \
                    + '"DayEndMinute": ' + self.comblabel29.result().split(':')[2] + ',\n    ' \
                    + self.comblabel30.result() + ',\n    ' + self.comblabel31.result() + ',\n    ' \
                    + self.comblabel32.result() \
                    + '\n  },\n'
        with open('ServerGameSettings.json', 'w') as sgs:
            self.str_total = '{\n'+self.str1 + self.str2 + self.str3 + self.str4 + self.str5 + self.str6+self.str7+'\n}'
            sgs.write(self.str_total)

    def savedata2(self):
        self.str2 = '  ' + self.comblabel33.result() + ',\n  ' + self.comblabel34.result() + ',\n  ' + self.comblabel35.result() \
                    + ',\n  ' + self.comblabel36.result() + ',\n  ' + self.comblabel37.result() + ',\n  ' \
                    + self.comblabel38.result() + ',\n  ' \
                    + '"PlayerInteractionSettings": {\n    ' \
                    + self.comblabel39.result() + ',\n    ' \
                    + '"VSPlayerWeekdayTime": {\n      ' \
                    + '"StartHour": ' + self.comblabel40.result().split(':')[1] + ',\n      ' \
                    + '"StartMinute": ' + self.comblabel40.result().split(':')[2] + ',\n      ' \
                    + '"EndHour": ' + self.comblabel41.result().split(':')[1] + ',\n      ' \
                    + '"EndMinute": ' + self.comblabel41.result().split(':')[2] + '\n    },\n    ' \
                    + '"VSPlayerWeekendTime": {\n      ' \
                    + '"StartHour": ' + self.comblabel42.result().split(':')[1] + ',\n      ' \
                    + '"StartMinute": ' + self.comblabel42.result().split(':')[2] + ',\n      ' \
                    + '"EndHour": ' + self.comblabel43.result().split(':')[1] + ',\n      ' \
                    + '"EndMinute": ' + self.comblabel43.result().split(':')[2] + '\n    },\n    ' \
                    + '"VSCastleWeekdayTime": {\n      ' \
                    + '"StartHour": ' + self.comblabel40.result().split(':')[1] + ',\n      ' \
                    + '"StartMinute": ' + self.comblabel40.result().split(':')[2] + ',\n      ' \
                    + '"EndHour": ' + self.comblabel41.result().split(':')[1] + ',\n      ' \
                    + '"EndMinute": ' + self.comblabel41.result().split(':')[2] + '\n    },\n    ' \
                    + '"VSCastleWeekendTime": {\n      ' \
                    + '"StartHour": ' + self.comblabel42.result().split(':')[1] + ',\n      ' \
                    + '"StartMinute": ' + self.comblabel42.result().split(':')[2] + ',\n      ' \
                    + '"EndHour": ' + self.comblabel43.result().split(':')[1] + ',\n      ' \
                    + '"EndMinute": ' + self.comblabel43.result().split(':')[2] + '\n    }\n  },\n'
        with open('ServerGameSettings.json', 'w') as sgs:
            self.str_total = '{\n' + self.str1 + self.str2 + self.str3 + self.str4 + self.str5 + self.str6 + self.str7 + '\n}'
            sgs.write(self.str_total)

    def savedata3(self):
        self.str3 = '  ' + self.comblabel48.result() + ',\n  ' + self.comblabel49.result() + ',\n  ' \
               + self.comblabel50.result() \
               + ',\n  ' + self.comblabel51.result() + ',\n  ' \
               + '"VampireStatModifiers": {\n    ' \
               + self.comblabel52.result() + ',\n    ' \
               + self.comblabel53.result() + ',\n    ' \
               + self.comblabel54.result() + ',\n    ' \
               + self.comblabel55.result() + ',\n    ' \
               + self.comblabel56.result() + ',\n    ' \
               + self.comblabel57.result() + ',\n    ' \
               + self.comblabel58.result() + ',\n    ' \
               + self.comblabel59.result() + '\n  },\n  ' \
               + self.comblabel60.result() + ',\n  ' + self.comblabel61.result() + ',\n  '
        with open('ServerGameSettings.json', 'w') as sgs:
            self.str_total = '{\n'+self.str1 + self.str2 + self.str3 + self.str4 + self.str5 + self.str6+self.str7+'\n}'
            sgs.write(self.str_total)

    def savedata4(self):
        self.str4 = '  ' + self.comblabel62.result() + ',\n  ' \
               +'"EquipmentStatModifiers_Global": {\n    '\
               + self.comblabel63.result() + ',\n    ' \
               + self.comblabel64.result() + ',\n    ' \
               + self.comblabel65.result() + ',\n    ' \
               + self.comblabel66.result() + ',\n    ' \
               + self.comblabel67.result() + ',\n    ' \
               + self.comblabel68.result() + ',\n    ' \
               + self.comblabel69.result() + '\n  },\n'
        with open('ServerGameSettings.json', 'w') as sgs:
            self.str_total = '{\n'+self.str1 + self.str2 + self.str3 + self.str4 + self.str5 + self.str6+self.str7+'\n}'
            sgs.write(self.str_total)

    def savedata5(self):
        self.str5 = '  ' + self.comblabel70.result() + ',\n  ' + self.comblabel71.result() \
               + ',\n  ' + self.comblabel72.result() + ',\n  ' + self.comblabel73.result() + ',\n  ' + self.comblabel74.result() \
               + ',\n  ' + self.comblabel75.result() + ',\n  ' + self.comblabel76.result() + ',\n  ' + self.comblabel77.result() \
               + ',\n  ' + self.comblabel78.result() + ',\n  ' + self.comblabel79.result() + ',\n  ' + self.comblabel80.result() \
               + ',\n  ' + self.comblabel81.result() + ',\n  ' + self.comblabel82.result() + ',\n  '
        with open('ServerGameSettings.json', 'w') as sgs:
            self.str_total = '{\n'+self.str1 + self.str2 + self.str3 + self.str4 + self.str5 + self.str6+self.str7+'\n}'
            sgs.write(self.str_total)

    def savedata6(self):
        self.str6 = '  "UnitStatModifiers_Global": {\n    ' + self.comblabel83.result() + ',\n    ' \
               + self.comblabel84.result() + '\n  },\n  ' \
               + '"UnitStatModifiers_VBlood": {\n    ' + self.comblabel85.result() + ',\n    ' \
               + self.comblabel86.result() + '\n  ' + '},\n'
        with open('ServerGameSettings.json', 'w') as sgs:
            self.str_total = '{\n'+self.str1 + self.str2 + self.str3 + self.str4 + self.str5 + self.str6+self.str7+'\n}'
            sgs.write(self.str_total)

    def enble(self):
        if self.comblabel1.comboBox.currentIndex() == 1:
            self.tab_2.setEnabled(0)
            self.pushButton2.setGeometry(QRect(30,500,300,35))
            self.pushButton2.setText('人机模式无需设置攻守城属性')
            if self.comblabel2.comboBox.count() == 2:
                self.comblabel2.comb_list.append('TimeRestricted')
                self.comblabel2.comboBox.addItem('人机模式此选项无效')
                self.comblabel2.comboBox.setCurrentText('人机模式此选项无效')
            self.comblabel2.comboBox.setEnabled(0)

        else:
            self.tab_2.setEnabled(1)
            self.pushButton2.setText('保存以上设置')
            self.pushButton2.setGeometry(QRect(130, 500, 100, 35))
            self.pushButton2.setStyleSheet(u"border-color: rgb(85, 0, 255);color: rgb(0, 0, 255);")
            self.comblabel2.comboBox.setEnabled(1)
            if self.comblabel2.comboBox.count() == 3:
                self.comblabel2.comboBox.clear()
                self.comblabel2.comb_list = self.comblabel2.comb_list[:2]
                self.comblabel2.comboBox.addItems(['全天可PK', '限时PK'])


    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("游戏相关设置")
        self.groupBox.setTitle("游戏系数设置")
        self.groupBox2.setTitle("攻守城堡设置")
        self.groupBox3.setTitle("角色属性设置")
        self.groupBox4.setTitle("装备属性设置")
        self.groupBox5.setTitle("材料生产制造设置")
        self.groupBox6.setTitle("怪物属性设置")
        self.groupBox7.setTitle("任务/研究/VBoss解锁设置:")

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_1), "游戏系数设置")
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), "攻守城堡设置")
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), "角色属性设置")
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4),
                                  "装备/材料/生产属性设置")
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_5),
                                  "任务/研究/Boss解锁等级设置")
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_6), "怪物和AI属性设置")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程

# 点击保存按钮相应生成str1,str2,str3,str4,str5,str6,str7的阶断性代码
