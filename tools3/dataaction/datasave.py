# -*- coding: utf-8 -*-
import os.path
import shutil
import sys
import time
from PySide6.QtCore import QMetaObject, QRect, Qt
from PySide6.QtGui import QFont
from PySide6.QtWidgets import QApplication, QGroupBox, QMainWindow, QWidget, QLabel, QPushButton, QMessageBox


class Ui_MainWindow(QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(700, 500)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("游戏和服务器参数的备份")
        self.groupBox.setGeometry(QRect(50, 30, 601, 181))
        font1 = QFont()
        font1.setPointSize(14)
        font1.setBold(True)
        self.groupBox.setFont(font1)

        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(20, 20, 560, 150))
        self.label.setWordWrap(True)
        font2 = QFont()
        font2.setPointSize(13)
        self.label.setFont(font2)
        self.label.setStyleSheet(u"color: rgb(0, 0, 255);")

        self.groupBox_2 = QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName("玩家游戏数据的备份")
        self.groupBox_2.setGeometry(QRect(50, 220, 601, 221))
        self.groupBox_2.setFont(font1)
        self.label2 = QLabel(self.groupBox_2)
        self.label2.setObjectName(u"label")
        self.label2.setGeometry(QRect(20, 30, 560, 80))
        self.label2.setWordWrap(True)
        font2 = QFont()
        font2.setPointSize(13)
        self.label2.setFont(font2)
        self.label2.setStyleSheet(u"color: rgb(255, 0, 0);")

        self.label3 = QLabel(self.groupBox_2)
        self.label3.setObjectName(u"label")
        self.label3.setGeometry(QRect(110, 150, 150, 30))
        self.label3.setFont(font2)
        self.label3.setStyleSheet(u"color: rgb(255, 0, 0);")

        self.pushButton = QPushButton(self.groupBox_2)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(310, 150, 100, 30))
        self.pushButton.setFont(font2)
        self.pushButton.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.pushButton.clicked.connect(self.savegamedata)

        MainWindow.setCentralWidget(self.centralwidget)
        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)

    def savegamedata(self):
        path1 = "C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\Saves\\v1"
        if os.path.exists(path1):
            file = f'备份的数据{time.strftime("%m%d%H%M%S")}'
            shutil.copytree(path1,file)
            QMessageBox.information(self,'备份成功提示:','恭喜!!!已在工具所在目录下生成游戏数据备份,文件名为:数据备份+备份日期(月日时分秒)')
        else:
            QMessageBox.information(self, '备份提示:',
                                    '游戏服务端目录下没有找到备份数据,或指定路径为非系统生成默认路径(请手动备份)!')

    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("数据备份:")
        self.groupBox.setTitle("游戏和服务器参数的备份:")
        self.groupBox_2.setTitle("玩家游戏数据的备份:")
        self.label.setText("       在完成对游戏参数和服务器参数设置并点击保存之后会在工具所在的目录下自动生成备份的文档."
                           "这里就不提供额外的备份接口了")
        self.label2.setText("       备份数据用于官方指定游戏更新时,防止更新时出现服务器再启动错误或在特定情况下想回滚游戏进度所提供的保护"
                            "手段!若是更新备份,请在停服后再进行备份!")
        self.label3.setText("备份游戏进程数据:")
        self.pushButton.setText("点击备份")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程