# -*- coding: utf-8 -*-
import os.path
import shutil
import sys
from PySide6.QtCore import QRect, Qt, QMetaObject, QThread
from PySide6.QtGui import QFont
from PySide6.QtWidgets import QApplication, QGroupBox, QMainWindow, QWidget, QLabel, QPushButton, QMessageBox


class Down_Thread(QThread):
    def __init__(self):
        super(Down_Thread, self).__init__()

    def run(self):
        win32api.ShellExecute(0, 'open', 'start_server.bat', '',
                              'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer', 1)


class Ui_MainWindow(QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(700, 500)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        MainWindow.setWindowTitle("数据备份:")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("游戏和服务器参数的备份")
        self.groupBox.setGeometry(QRect(30, 30, 640, 400))
        font1 = QFont()
        font1.setPointSize(14)
        font1.setBold(True)
        self.groupBox.setFont(font1)
        self.groupBox.setTitle("回滚游戏玩家数据(请按步骤指引操作):")

        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(20, 40, 360, 30))
        font2 = QFont()
        font2.setPointSize(13)
        self.label.setFont(font2)
        self.label.setStyleSheet(u"color: rgb(255, 0, 0);")
        self.label.setText("第一步, 点击右侧按钮生成准备回滚数据容器:")

        self.label2 = QLabel(self.groupBox)
        self.label2.setObjectName(u"label")
        self.label2.setGeometry(QRect(20, 110, 500, 30))
        self.label2.setFont(font2)
        self.label2.setStyleSheet(u"color: rgb(255, 0, 0);")
        self.label2.setText("第二步, 检查本工具所在目录是否有数据容器文件夹生成.")

        self.label3 = QLabel(self.groupBox)
        self.label3.setObjectName(u"label")
        self.label3.setGeometry(QRect(20, 180, 600, 30))
        self.label3.setFont(font2)
        self.label3.setStyleSheet(u"color: rgb(255, 0, 0);")
        self.label3.setText("第三步, 将备份的数据文件(有且只能放入一份数据文件)移至数据容器文件夹下.")

        self.label4 = QLabel(self.groupBox)
        self.label4.setObjectName(u"label")
        self.label4.setGeometry(QRect(20, 250, 500, 30))
        self.label4.setFont(font2)
        self.label4.setStyleSheet(u"color: rgb(255, 0, 0);")
        self.label4.setText("第四步, 自动替换服务端下现有数据(建议操作前先备份现有数据).")

        self.label5 = QLabel(self.groupBox)
        self.label5.setObjectName(u"label")
        self.label5.setGeometry(QRect(20, 320, 450, 30))
        self.label5.setFont(font2)
        self.label5.setStyleSheet(u"color: rgb(255, 0, 0);")
        self.label5.setText("第五步, 右侧按钮为可点击状态后,可点击启动服务端.")

        self.pushButton2 = QPushButton(self.groupBox)
        self.pushButton2.setObjectName(u"pushButton")
        self.pushButton2.setGeometry(QRect(450, 320, 120, 30))
        font = QFont()
        font.setPointSize(12)
        self.pushButton2.setFont(font)
        self.pushButton2.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.pushButton2.clicked.connect(self.start_game)
        self.pushButton2.setText("点击启动")

        self.pushButton1 = QPushButton(self.groupBox)
        self.pushButton1.setObjectName(u"pushButton")
        self.pushButton1.setGeometry(QRect(510, 250, 100, 30))
        font = QFont()
        font.setPointSize(12)
        self.pushButton1.setFont(font)
        self.pushButton1.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.pushButton1.clicked.connect(self.remove_data_file)
        self.pushButton1.setText("点击替换")

        self.pushButton = QPushButton(self.groupBox)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(400, 40, 120, 30))
        font = QFont()
        font.setPointSize(12)
        self.pushButton.setFont(font)
        self.pushButton.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.pushButton.clicked.connect(self.create_data_file)
        self.pushButton1.clicked.connect(self.remove_data_file)
        self.pushButton.setText("点击生成")

        MainWindow.setCentralWidget(self.centralwidget)
        QMetaObject.connectSlotsByName(MainWindow)

    def create_data_file(self):
        if not os.path.exists('./数据容器文件夹'):
            os.mkdir('./数据容器文件夹')
        self.pushButton.setDisabled(True)

    def remove_data_file(self):
        if not os.path.exists('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\Saves'):
            os.makedirs('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\Saves')
        os.listdir('./数据容器文件夹')
        lis = os.listdir('./数据容器文件夹')
        if len(lis) != 1:
            QMessageBox.warning(self,'数据有误','当前数据容器文件夹下没有放入或放入了多份备份的数据文件!!!请重新准备好备份数据!!!')
        else:
            os.rename(f'./数据容器文件夹/{lis[0]}', './数据容器文件夹/v1')
            if os.path.exists('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\Saves\\v1'):
                shutil.rmtree('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\Saves\\v1')
            shutil.copytree('./数据容器文件夹/v1',
                            'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\Saves/v1')
            self.pushButton1.setDisabled(True)
            QMessageBox.information(self,'替换完成:','数据替换已完成,可以进行下一步操作!')

    def start_game(self):
        if os.path.exists('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\start_server.bat'):
            self.pushButton2.setDisabled(True)
            self.infobox = QMessageBox.information(self, '启动提示:',
                                                   '游戏服务端已启动,稍后即可以进入游戏,开服过程中,交互窗口不能关闭!')
            self.down_thread = Down_Thread()
            self.down_thread.start()
        else:
            self.infobox = QMessageBox.information(self, '启动报错:',
                                                   '游戏设置和服务器设置没有完成,前完成设置再启动游戏服务端!')




if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程