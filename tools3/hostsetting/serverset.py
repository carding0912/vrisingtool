# -*- coding: utf-8 -*-
import sys

from PySide6.QtCore import QRect, QMetaObject
from PySide6.QtGui import QIcon, QPixmap, QFont, Qt
from PySide6.QtWidgets import QMainWindow, QWidget, QGroupBox, QSpinBox, QLabel, QTextEdit, QLineEdit, QPushButton, \
    QMessageBox, QApplication
from images import images
from public.makelabel import SpinLabel, TextLabel, CombLabel

font1 = QFont()
font1.setPointSize(14)
font1.setBold(False)
font2 = QFont()
font2.setPointSize(14)
font3 = QFont()
font3.setPointSize(11)


class Ui_MainWindow(QMainWindow):

    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        icon = QIcon()
        icon.addPixmap(QPixmap(":/set/set.jpg"), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        font = QFont()
        font.setFamily("微软雅黑")
        font.setPointSize(10)
        MainWindow.setFont(font)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("服务器设置弹框")
        self.portBox = QGroupBox(self.centralwidget)
        self.portBox.setGeometry(QRect(50, 10, 660, 160))
        self.portBox.setFont(font1)
        self.portBox.setStyleSheet("color: rgb(255, 0, 0);")
        self.portBox.setObjectName("端口设置分组框")

        self.portSpinBox = QSpinBox(self.portBox)
        self.portSpinBox.setGeometry(QRect(140, 35, 120, 30))
        self.portSpinBox.setFont(font1)
        self.portSpinBox.setStyleSheet("color: rgb(0, 0, 0);")
        self.portSpinBox.setAlignment(Qt.AlignCenter)
        self.portSpinBox.setMinimum(5000)
        self.portSpinBox.setMaximum(65535)
        self.portSpinBox.setProperty("value", 9876)
        self.portSpinBox.setObjectName("Port设置选择标签")

        self.querySpinBox = QSpinBox(self.portBox)
        self.querySpinBox.setGeometry(QRect(450, 35, 120, 30))
        self.querySpinBox.setFont(font1)
        self.querySpinBox.setStyleSheet("color: rgb(0, 0, 0);")
        self.querySpinBox.setAlignment(Qt.AlignCenter)
        self.querySpinBox.setMinimum(5000)
        self.querySpinBox.setMaximum(65535)
        self.querySpinBox.setProperty("value", 9877)
        self.querySpinBox.setObjectName("QueryPort设置选择标签")

        self.portlabel = QLabel(self.portBox)
        self.portlabel.setGeometry(QRect(60, 35, 60, 30))
        self.portlabel.setFont(font2)
        self.portlabel.setObjectName("Port标签")

        self.queryportlabel = QLabel(self.portBox)
        self.queryportlabel.setGeometry(QRect(320, 35, 120, 30))
        self.queryportlabel.setFont(font2)
        self.queryportlabel.setObjectName("QueryPort标签")

        self.textexplan = QTextEdit(self.portBox)
        self.textexplan.setEnabled(False)
        self.textexplan.setGeometry(QRect(10, 70, 641, 70))
        self.textexplan.setFont(font)
        self.textexplan.setObjectName("端口设置解释")
        self.textexplan.setStyleSheet(u"color: rgb(0, 0, 255);")

        self.titleBox = QGroupBox(self.centralwidget)
        self.titleBox.setGeometry(QRect(50, 165, 660, 100))
        self.titleBox.setFont(font2)
        self.titleBox.setStyleSheet("color: rgb(255, 0, 0);")
        self.titleBox.setObjectName("标题设置的分组框")

        self.titlelabel = QLabel(self.titleBox)
        self.titlelabel.setGeometry(QRect(0, 30, 220, 25))
        self.titlelabel.setFont(font3)
        self.titlelabel.setStyleSheet("color: rgb(255, 0, 0);")
        self.titlelabel.setObjectName("标题设置提示标签")

        self.server_title = QLineEdit(self.titleBox)
        self.server_title.setGeometry(QRect(220, 30, 420, 25))
        self.server_title.setFont(font3)
        self.server_title.setStyleSheet("color: rgb(0, 0, 255);")
        self.server_title.setObjectName("标题输入文本编辑标签")

        self.descritionlabel = QLabel(self.titleBox)
        self.descritionlabel.setGeometry(QRect(0, 65, 220, 25))
        self.descritionlabel.setFont(font3)
        self.descritionlabel.setStyleSheet("color: rgb(255, 0, 0);")
        self.descritionlabel.setObjectName("标题设置提示标签")

        self.server_descrition = QLineEdit(self.titleBox)
        self.server_descrition.setGeometry(QRect(140, 65, 500, 25))
        self.server_descrition.setFont(font3)
        self.server_descrition.setStyleSheet("color: rgb(0, 0, 255);")
        self.server_descrition.setObjectName("标题输入文本编辑标签")

        self.othersetBox = QGroupBox(self.centralwidget)
        self.othersetBox.setGeometry(QRect(50, 275, 671, 260))
        self.othersetBox.setFont(font2)
        self.othersetBox.setStyleSheet("color: rgb(255, 0, 0);")
        self.othersetBox.setObjectName("服务器其它设置的分组框")

        self.maxonline = SpinLabel(self.othersetBox, 10, 30, 'MaxConnectedUsers:', '在线玩家上限:', 40, 1, 200, 10)
        self.maxgmonline = SpinLabel(self.othersetBox, 340, 30, 'MaxConnectedAdmins:', '在线管理员上限:', 4, 0, 9999,
                                     1)
        self.serverfps = SpinLabel(self.othersetBox, 10, 70, 'ServerFps:', '服务端Fps:', 30, 10, 500, 30)
        self.savename = TextLabel(self.othersetBox, 340, 70, 'SaveName', '数据文件名(不支持中文):', 'world1')
        self.password = TextLabel(self.othersetBox, 10, 110, 'Password', '服务端加密码:', '')
        self.secure = CombLabel(self.othersetBox, 340, 110, 'Secure:', '服务器保护:',
                                ['true', 'false'], ['保护', '不保护'])
        self.servershow = CombLabel(self.othersetBox, 10, 150, 'ListOnMasterServer:', '列表显示本服务器:',
                                    ['true', 'false'], ['显示', '不显示'])
        self.savecount = SpinLabel(self.othersetBox, 340, 150, 'AutoSaveCount:', '自动保存数量:', 50, 10, 210, 10)
        self.savetime = SpinLabel(self.othersetBox, 10, 190, 'AutoSaveInterval:', '自动保存时间间隔:', 600, 1, 9999,
                                  60)
        self.gmdebug = CombLabel(self.othersetBox, 340, 190, 'AdminOnlyDebugEvents:', '管理员调试事件:',
                                 ['true', 'false'], ['允许', '不允许'])
        self.disabledebug = CombLabel(self.othersetBox, 10, 230, 'DisableDebugEvents:', '禁用调试事件:',
                                      ['true', 'false'], ['禁用', '允许'])

        self.saveSetButton = QPushButton(self.centralwidget)
        self.saveSetButton.setGeometry(QRect(320, 545, 141, 41))
        self.saveSetButton.setFont(font1)
        self.saveSetButton.setStyleSheet("color: rgb(0, 0, 255);")
        self.saveSetButton.setObjectName("保存设置的按钮")
        self.saveSetButton.clicked.connect(self.savehostset)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)

    def savehostset(self):
        if self.portSpinBox.value() == self.querySpinBox.value():
            self.querySpinBox.setProperty("value", 9877)
            self.portSpinBox.setProperty("value", 9876)
            QMessageBox.warning(self, '警告', '两个端口号不能相同,已恢复官方默认设置', QMessageBox.StandardButton.Ok)
        if self.server_title.text() == '':
            self.server_title.setText('My V Rising Server')
            QMessageBox.warning(self, '警告', '服务器名不可以为空,否则不会显示服务器', QMessageBox.StandardButton.Ok)
        if self.savename.result() == '"SaveName": ""':
            self.savename.textedit.setText('world1')
            QMessageBox.warning(self, '警告', '数据保存文件夹名不可以为空,否则无法开启服务器', QMessageBox.StandardButton.Ok)

        self.Rcon = """
  "Rcon":{
  "Enabled":false,
  "Port":25575,
  "Password":""
  }
"""
        self.hoststr = '{\n  "Name": "' + self.server_title.text() + '",\n  ' \
                       + '"Description": "' + self.server_descrition.text() + '",\n  ' \
                       + '"Port": ' + str(self.portSpinBox.value()) + ',\n  ' \
                       + '"QueryPort": ' + str(self.querySpinBox.value()) + ',\n  ' \
                       + self.maxonline.result() + ',\n  ' + self.maxgmonline.result() + ',\n  ' \
                       + self.serverfps.result() + ',\n  ' + self.savename.result() + ',\n  ' \
                       + self.password.result() + ',\n  ' + self.secure.result() + ',\n  ' \
                       + self.servershow.result() + ',\n  ' + self.savecount.result() + ',\n  ' \
                       + self.savetime.result() + ',\n  ' + '"GameSettingsPreset":"",\n  ' \
                       + self.gmdebug.result() + ',\n  ' + self.disabledebug.result() + ',\n  ' + self.Rcon + '}\n'
        self.server_title_text = self.server_title.text()
        self.savename_text = self.savename.result().strip('"SaveName": ')

        with open('ServerHostSettings.json', 'w') as sgs:
            sgs.write(self.hoststr)
        with open('title.txt','w',encoding='utf-8')as datatxt:
            datatxt.write(self.server_title_text)
        with open('savename.txt', 'w', encoding='utf-8') as savenametxt:
            savenametxt.write(self.savename_text)


    def retranslateUi(self, MainWindow):

        MainWindow.setWindowTitle("服务端相关设置")
        self.portBox.setTitle("游戏通信端口设置:")
        self.portlabel.setText("\"Port\":")
        self.queryportlabel.setText("\"QueryPort\":")
        self.textexplan.setText('    官方默认端口为:9876和9877,可更改,设置端口后要打开服务器端口的TCP/UDP通'
                                                         '信,此处设置是指定游戏服务文件的端口,更改后会改变游戏客户端的连接地址,采用'
                                                         '默认设置可直接输IP连接游戏,其它设置游戏客户端连接时需输入IP+端口号(Port).')
        self.titleBox.setTitle("服务器标题设置:")
        self.titlelabel.setText("服务器名称(客户端列表显示名):")
        self.othersetBox.setTitle("服务器的其它设置:")
        self.saveSetButton.setText("保存以上设置")
        self.descritionlabel.setText("服务器详情页面公告:")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程
