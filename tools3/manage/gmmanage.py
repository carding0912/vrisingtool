# -*- coding: utf-8 -*-
import sys

import main
from PySide6.QtCore import QRect, QSize, Qt, QDir
from PySide6.QtGui import QFont, QIcon
from PySide6.QtWidgets import QApplication, QGroupBox, QLabel, QLineEdit, QMainWindow, QPushButton, QWidget, \
    QMessageBox, QTableWidget, QTableWidgetItem, QAbstractItemView
from images import images


class Ui_MainWindow(QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(500, 600)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        icon = QIcon()
        icon.addFile(u":/set/set.jpg", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setGeometry(QRect(10, 30, 480, 550))
        font = QFont()
        font.setPointSize(14)
        font.setBold(True)
        self.groupBox.setFont(font)
        self.groupBox.setStyleSheet(u"color: rgb(0, 0, 127);")
        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(30, 40, 115, 21))
        font1 = QFont()
        font1.setPointSize(11)
        self.label.setFont(font1)

        self.steamID = QLineEdit(self.groupBox)
        self.steamID.setObjectName(u"steamID")
        self.steamID.setGeometry(QRect(50, 70, 190, 25))
        font2 = QFont()
        font2.setPointSize(10)
        self.steamID.setFont(font2)

        self.label3 = QLabel(self.groupBox)
        self.label3.setObjectName(u"label")
        self.label3.setGeometry(QRect(30, 430, 115, 21))
        font1 = QFont()
        font1.setPointSize(11)
        self.label3.setFont(font1)

        self.delsteamID = QLineEdit(self.groupBox)
        self.delsteamID.setObjectName(u"steamID")
        self.delsteamID.setGeometry(QRect(30, 460, 190, 25))
        font2 = QFont()
        font2.setPointSize(10)
        self.delsteamID.setFont(font2)

        self.name = QLineEdit(self.groupBox)
        self.name.setObjectName(u"name")
        self.name.setGeometry(QRect(270, 70, 150, 25))
        font3 = QFont()
        font3.setPointSize(9)
        self.name.setFont(font3)
        self.addman = QPushButton(self.groupBox)
        self.addman.setObjectName(u"addman")
        self.addman.setGeometry(QRect(360, 120, 85, 25))
        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(30, 180, 200, 21))
        self.label_2.setFont(font1)

        self.del_btn = QPushButton(self.groupBox)
        self.del_btn.setObjectName(u"del_man")
        self.del_btn.setGeometry(QRect(270, 460, 85, 25))
        MainWindow.setCentralWidget(self.centralwidget)
        self.addman.clicked.connect(self.savesteamID)
        self.del_btn.clicked.connect(self.deletelsteamID)
        self.retranslateUi(MainWindow)
        self.showGM()
        # QMetaObject.connectSlotsByName(MainWindow)

    def deletelsteamID(self):
        delsteamID = self.delsteamID.text()
        if delsteamID:
            with open('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\adminlist.txt',
                      'r', encoding='utf-8') as delf:
                id_total = delf.readlines()
                new_total = ''
                for i in id_total:
                    if i.strip('\n') != delsteamID:
                        new_total += i

            with open('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\adminlist.txt',
                      'w', encoding='utf-8') as delf:
                print(new_total)
                delf.write(new_total)

            with open(
                    'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\VRisingServer_Data\\StreamingAssets\\Settings\\adminlist.txt',
                    'w', encoding='utf-8') as delf:
                print(new_total)
                delf.write(new_total)

            with open('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\Settings\\adminlist.txt',
                      'w', encoding='utf-8') as delf:
                print(new_total)
                delf.write(new_total)

            with open('GM管理系统文档.txt', 'r', encoding='utf-8') as sdf:
                mtotal = sdf.readlines()
                new_mtoal = ''
                for i in mtotal:
                    if i == '\n' or i.startswith(delsteamID.strip(' ')):
                        pass
                    else:
                        new_mtoal += i
            with open('GM管理系统文档.txt', 'w', encoding='utf-8') as sdf:
                sdf.write(new_mtoal)
            QMessageBox.information(self, '解除成功:', f'steamID为:{delsteamID}的玩家已被解除权限')
            main.Ui_MainWindow.gmmanage(self)
            self.close()
        else:
            QMessageBox.information(self, '解除失败:', f'没有指定相关人员的steamID号')

    def showGM(self):
        path = 'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\'
        self.dir = QDir(path)
        if not self.dir.exists():
            self.dir.mkdir(path)
        f1 = open('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\adminlist.txt',
                  'a+', encoding='utf-8')
        f1.close()

        f2 = open('GM管理系统文档.txt', 'a+', encoding='utf-8')
        f2.close()

        with open('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\adminlist.txt',
                  'r', encoding='utf-8') as sfim:
            self.s_total = sfim.readlines()
            self.idnamelist = []

        with open('GM管理系统文档.txt', 'r', encoding='utf-8') as sdf:
            self.txt_s_total = sdf.readlines()
        if self.s_total and self.txt_s_total:
            self.txt_s_total_dict = {}
            for i in self.txt_s_total:
                if i != '' and i != '\n':
                    li = i.split(':')
                    self.txt_s_total_dict[li[0].strip('\n')] = li[1].strip('\n')
            self.row = len(self.s_total)
            self.gmtable = QTableWidget(self.row, 2, self.groupBox)
            self.gmtable.setGeometry(QRect(30, 210, 350, 200))
            self.gmtable.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers)
            self.gmtable.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
            font = QFont()
            font.setPointSize(10)
            self.gmtable.setFont(font)
            self.gmtable.setHorizontalHeaderLabels(['SteamID', '备注名'])
            for i in self.s_total:
                self.idnamelist.append(i.strip('\n'))
            for n in range(self.row):
                steamid = self.idnamelist[n]
                name = self.txt_s_total_dict.get(steamid)
                self.gmtable.setItem(n, 0, QTableWidgetItem(steamid))
                self.gmtable.setItem(n, 1, QTableWidgetItem(name))
                self.gmtable.resizeColumnsToContents()
                self.gmtable.horizontalHeader().setStretchLastSection(True)

        else:
            self.label_none = QLabel(self.groupBox)
            self.label_none.setObjectName('无管理员')
            self.label_none.setGeometry(30, 210, 200, 30)
            font = QFont()
            font.setPointSize(10)
            self.label_none.setFont(font)
            self.label_none.setText('游戏中还没有设置管理员!')

    def savesteamID(self):
        self.addman.setDisabled(True)
        steamid = self.steamID.text()
        name = self.name.text()
        if steamid:
            if name == '':
                name = '没有备注或角色名'
            self.steamID.clear()
            self.name.clear()
            path = 'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\'
            path1 = 'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\VRisingServer_Data\\StreamingAssets\\Settings\\'
            path2 = 'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\Settings\\'
            self.dir = QDir(path)
            self.dir1 = QDir(path1)
            self.dir2 = QDir(path2)

            if not (self.dir.exists() and self.dir2.exists() and self.dir1.exists()):
                self.dir.mkdir(path)
                self.dir1.mkdir(path1)
                self.dir2.mkdir(path2)
            with open('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\adminlist.txt',
                      'a', encoding='utf-8') as idw:
                idw.write(str(steamid))
                idw.write('\n')
            with open(
                    'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\VRisingServer_Data\\StreamingAssets\\Settings\\adminlist.txt',
                    'a', encoding='utf-8') as idw1:
                idw1.write(str(steamid))
                idw1.write('\n')
            with open('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\Settings\\adminlist.txt',
                      'a', encoding='utf-8') as idw2:
                idw2.write(str(steamid))
                idw2.write('\n')

            with open('GM管理系统文档.txt', 'a', encoding='utf-8') as idw:
                stridname = f'{steamid}:{name}\n'
                idw.write(stridname)
            QMessageBox.information(self, '添加成功:', f'steamID为:{steamid}的玩家已成为游戏管理员')
            self.addman.setDisabled(False)
            main.Ui_MainWindow.gmmanage(self)
            self.close()
        else:
            QMessageBox.information(self, '添加失败:', 'SteamID号为必填项,不可缺少!')
            self.addman.setDisabled(False)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(
            "游戏管理员管理系统")
        self.groupBox.setTitle("GM管理系统:")
        self.label.setText(
            "添加游戏管理员:")
        self.steamID.setPlaceholderText("此处输入steamID号(必填项)")
        self.delsteamID.setPlaceholderText("此处输入steamID号(必填项)")
        self.name.setPlaceholderText("备注名/角色名(选填项)")
        self.addman.setText("提交")
        self.label_2.setText("当前游戏中已存在的管理员:")
        self.label3.setText("解除管理员权限:")
        self.del_btn.setText("点击解除")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程
