DuoPvP = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",
  "ClanSize": 2,

  "DropTableModifier_General": 1.25,
  "DropTableModifier_Missions": 1.25,
  "MaterialYieldModifier_Global": 1.25,
  "BloodEssenceYieldModifier": 1.25,
  "BloodDrainModifier": 1.25,

  "CastleStatModifiers_Global": {
    "CastleLimit": 2
  }
}
"""

HardcoreDouPvP = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartClaimMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeSeizedOrDestroyedByPlayers",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": false,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",
  "Death_DurabilityFactorLoss": 0.0,
  "Death_DurabilityLossFactorAsResources": 0.0,
  "ClanSize": 2,

  "CastleHeartSafetyBoxLimit": 0,

  "DropTableModifier_General": 1.25,
  "DropTableModifier_Missions": 1.25,
  "MaterialYieldModifier_Global": 1.25,
  "BloodEssenceYieldModifier": 1.25,
  "BloodDrainModifier": 1.25,

  "CastleStatModifiers_Global": {
    "CastleLimit": 2
  }
}
"""

HardcorePvP = """
{
  "GameModeType": "PvP",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeSeizedOrDestroyedByPlayers",
  "CastleHeartClaimMode": "TimeRestricted",
  "BloodBoundEquipment": false,
  "Death_DurabilityFactorLoss": 0.0,
  "Death_DurabilityLossFactorAsResources": 0.0,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",

  "CastleHeartSafetyBoxLimit": 0,

  "DropTableModifier_General": 1.25,
  "DropTableModifier_Missions": 1.25,
  "MaterialYieldModifier_Global": 1.25,
  "BloodEssenceYieldModifier": 1.25,
  "BloodDrainModifier": 1.25,

  "CastleStatModifiers_Global": {
    "CastleLimit": 1
  }

}
"""

Level30PvE = """
{
  "GameModeType": "PvE",
  "DeathContainerPermission": "ClanMembers",
  "RelicSpawnType": "Plentiful",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "CastleDamageMode": "Never",
  "BloodBoundEquipment": true,
  "CanLootEnemyContainers": false,
  "Death_DurabilityFactorLoss": 0.0,
  "Death_DurabilityLossFactorAsResources": 0.0,
  "AnnounceSiegeWeaponSpawn": false,
  "StarterEquipmentId": -376135143,
  "StarterResourcesId": -696202180,
  "VBloodUnitSettings": [
    {
      "UnitId": -1905691330,
      "UnitLevel": 16,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1124739990,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 2122229952,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2025101517,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 763273073,
      "UnitLevel": 26,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1106149033,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 577478542,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    }
  ],
  "UnlockedAchievements": [
    -1770927128,
    436375429,
    -1400391027,
    -2071097880,
    1695239324,
    1502386974,
    1694767961,
    -1899098914,
    560247139,
    -1995132640,
    -1434604634
  ],
  "UnlockedResearchs": [
    -495424062
  ]

}
"""

Level30PvP = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",
  "BloodDrainModifier": 1.25,

  // Not decided yet disabled
  //"DropTableModifier_General": 1.5,
  //"DropTableModifier_Missions": 1.5,
  //"MaterialYieldModifier_Global": 1.5,
  //"BloodEssenceYieldModifier": 1.5,
  "StarterEquipmentId": -376135143,
  "StarterResourcesId": -696202180,
  "VBloodUnitSettings": [
    {
      "UnitId": -1905691330,
      "UnitLevel": 16,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1124739990,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 2122229952,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2025101517,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 763273073,
      "UnitLevel": 26,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1106149033,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 577478542,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    }
  ],
  "UnlockedAchievements": [
    -1770927128,
    436375429,
    -1400391027,
    -2071097880,
    1695239324,
    1502386974,
    1694767961,
    -1899098914,
    560247139,
    -1995132640,
    -1434604634
  ],
  "UnlockedResearchs": [
    -495424062
  ],
  "CastleStatModifiers_Global": {
    "CastleLimit": 1
  }
}
"""

Level50PvE = """
{
  "GameModeType": "PvE",
  "DeathContainerPermission": "ClanMembers",
  "RelicSpawnType": "Plentiful",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "CastleDamageMode": "Never",
  "BloodBoundEquipment": true,
  "CanLootEnemyContainers": false,
  "Death_DurabilityFactorLoss": 0.0,
  "Death_DurabilityLossFactorAsResources": 0.0,
  "AnnounceSiegeWeaponSpawn": false,
  "StarterEquipmentId": -1613823352,
  "StarterResourcesId": 481718792,
  "VBloodUnitSettings": [
    {
      "UnitId": -1905691330,
      "UnitLevel": 16,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1124739990,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 2122229952,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2025101517,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 763273073,
      "UnitLevel": 26,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1106149033,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 577478542,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2039908510,
      "UnitLevel": 30,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1896428751,
      "UnitLevel": 30,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -484556888,
      "UnitLevel": 34,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1391546313,
      "UnitLevel": 36,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 153390636,
      "UnitLevel": 37,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1659822956,
      "UnitLevel": 37,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1942352521,
      "UnitLevel": 38,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -29797003,
      "UnitLevel": 40,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -99012450,
      "UnitLevel": 44,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 939467639,
      "UnitLevel": 46,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1449631170,
      "UnitLevel": 46,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1065970933,
      "UnitLevel": 48,
      "DefaultUnlocked": true
    }
  ],
  "UnlockedAchievements": [
    -1770927128,
    436375429,
    -1400391027,
    -2071097880,
    1695239324,
    1502386974,
    1694767961,
    -1899098914,
    560247139,
    -1995132640,
    -1434604634,
    1668809517,
    334973636,
    606418711,
    -892747762,
    -437605270
  ],
  "UnlockedResearchs": [
    -495424062,
    -1292809886
  ]
}
"""

Level50PvP = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",
  "BloodDrainModifier": 1.25,

  // Not decided yet disabled
  //"DropTableModifier_General": 1.5,
  //"DropTableModifier_Missions": 1.5,
  //"MaterialYieldModifier_Global": 1.5,
  //"BloodEssenceYieldModifier": 1.5,
  "StarterEquipmentId": -1613823352,
  "StarterResourcesId": 481718792,
  "VBloodUnitSettings": [
    {
      "UnitId": -1905691330,
      "UnitLevel": 16,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1124739990,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 2122229952,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2025101517,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 763273073,
      "UnitLevel": 26,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1106149033,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 577478542,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2039908510,
      "UnitLevel": 30,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1896428751,
      "UnitLevel": 30,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -484556888,
      "UnitLevel": 34,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1391546313,
      "UnitLevel": 36,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 153390636,
      "UnitLevel": 37,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1659822956,
      "UnitLevel": 37,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1942352521,
      "UnitLevel": 38,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -29797003,
      "UnitLevel": 40,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -99012450,
      "UnitLevel": 44,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 939467639,
      "UnitLevel": 46,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1449631170,
      "UnitLevel": 46,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1065970933,
      "UnitLevel": 48,
      "DefaultUnlocked": true
    }
  ],
  "UnlockedAchievements": [
    -1770927128,
    436375429,
    -1400391027,
    -2071097880,
    1695239324,
    1502386974,
    1694767961,
    -1899098914,
    560247139,
    -1995132640,
    -1434604634,
    1668809517,
    334973636,
    606418711,
    -892747762,
    -437605270
  ],
  "UnlockedResearchs": [
    -495424062,
    -1292809886
  ],

  "CastleStatModifiers_Global": {
    "CastleLimit": 1
  }
}
"""

Level70PvE = """
{
  "GameModeType": "PvE",
  "DeathContainerPermission": "ClanMembers",
  "RelicSpawnType": "Plentiful",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "CastleDamageMode": "Never",
  "BloodBoundEquipment": true,
  "CanLootEnemyContainers": false,
  "Death_DurabilityFactorLoss": 0.0,
  "Death_DurabilityLossFactorAsResources": 0.0,
  "AnnounceSiegeWeaponSpawn": false,
  "StarterEquipmentId": -258598606,
  "StarterResourcesId": -766909665,
  "VBloodUnitSettings": [
    {
      "UnitId": -1905691330,
      "UnitLevel": 16,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1124739990,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 2122229952,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2025101517,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 763273073,
      "UnitLevel": 26,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1106149033,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 577478542,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2039908510,
      "UnitLevel": 30,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1896428751,
      "UnitLevel": 30,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -484556888,
      "UnitLevel": 34,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1391546313,
      "UnitLevel": 36,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 153390636,
      "UnitLevel": 37,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1659822956,
      "UnitLevel": 37,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1942352521,
      "UnitLevel": 38,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -29797003,
      "UnitLevel": 40,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -99012450,
      "UnitLevel": 44,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 939467639,
      "UnitLevel": 46,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1449631170,
      "UnitLevel": 46,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1065970933,
      "UnitLevel": 48,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 850622034,
      "UnitLevel": 52,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 24378719,
      "UnitLevel": 56,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1688478381,
      "UnitLevel": 58,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -680831417,
      "UnitLevel": 60,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -548489519,
      "UnitLevel": 60,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -203043163,
      "UnitLevel": 62,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1208888966,
      "UnitLevel": 62,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1968372384,
      "UnitLevel": 62,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1007062401,
      "UnitLevel": 64,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2013903325,
      "UnitLevel": 64,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 685266977,
      "UnitLevel": 68,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 114912615,
      "UnitLevel": 68,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1347412392,
      "UnitLevel": 68,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -910296704,
      "UnitLevel": 72,
      "DefaultUnlocked": true
    }
  ],
  "UnlockedAchievements": [
    -1770927128,
    436375429,
    -1400391027,
    -2071097880,
    1695239324,
    1502386974,
    1694767961,
    -1899098914,
    560247139,
    -1995132640,
    -1434604634,
    1668809517,
    334973636,
    606418711,
    -892747762,
    -437605270,
    -2104585843,
    -327597689
  ],
  "UnlockedResearchs": [
    -495424062,
    -1292809886,
    -1262194203
  ]
}
"""

Level70PvP = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",
  "BloodDrainModifier": 1.25,

  // Not decided yet disabled
  //"DropTableModifier_General": 1.5,
  //"DropTableModifier_Missions": 1.5,
  //"MaterialYieldModifier_Global": 1.5,
  //"BloodEssenceYieldModifier": 1.5,
  "StarterEquipmentId": -258598606,
  "StarterResourcesId": -766909665,
  "VBloodUnitSettings": [
    {
      "UnitId": -1905691330,
      "UnitLevel": 16,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1124739990,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 2122229952,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2025101517,
      "UnitLevel": 20,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 763273073,
      "UnitLevel": 26,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1106149033,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 577478542,
      "UnitLevel": 27,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2039908510,
      "UnitLevel": 30,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1896428751,
      "UnitLevel": 30,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -484556888,
      "UnitLevel": 34,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1391546313,
      "UnitLevel": 36,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 153390636,
      "UnitLevel": 37,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1659822956,
      "UnitLevel": 37,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1942352521,
      "UnitLevel": 38,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -29797003,
      "UnitLevel": 40,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -99012450,
      "UnitLevel": 44,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 939467639,
      "UnitLevel": 46,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1449631170,
      "UnitLevel": 46,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1065970933,
      "UnitLevel": 48,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 850622034,
      "UnitLevel": 52,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 24378719,
      "UnitLevel": 56,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 1688478381,
      "UnitLevel": 58,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -680831417,
      "UnitLevel": 60,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -548489519,
      "UnitLevel": 60,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -203043163,
      "UnitLevel": 62,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1208888966,
      "UnitLevel": 62,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1968372384,
      "UnitLevel": 62,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1007062401,
      "UnitLevel": 64,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -2013903325,
      "UnitLevel": 64,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 685266977,
      "UnitLevel": 68,
      "DefaultUnlocked": true
    },
    {
      "UnitId": 114912615,
      "UnitLevel": 68,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -1347412392,
      "UnitLevel": 68,
      "DefaultUnlocked": true
    },
    {
      "UnitId": -910296704,
      "UnitLevel": 72,
      "DefaultUnlocked": true
    }
  ],
  "UnlockedAchievements": [
    -1770927128,
    436375429,
    -1400391027,
    -2071097880,
    1695239324,
    1502386974,
    1694767961,
    -1899098914,
    560247139,
    -1995132640,
    -1434604634,
    1668809517,
    334973636,
    606418711,
    -892747762,
    -437605270,
    -2104585843,
    -327597689
  ],
  "UnlockedResearchs": [
    -495424062,
    -1292809886,
    -1262194203
  ],
  "CastleStatModifiers_Global": {
    "CastleLimit": 1
  }
}
"""

SoloPvP = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",
  "ClanSize": 1,

  "DropTableModifier_General": 1.5,
  "DropTableModifier_Missions": 1.5,
  "MaterialYieldModifier_Global": 1.5,
  "BloodEssenceYieldModifier": 1.5,
  "BloodDrainModifier": 1.25,

  "CastleStatModifiers_Global": {
    "CastleLimit": 2
  }
}
"""

StandardPvE = """
{
  "GameModeType": "PvE",
  "DeathContainerPermission": "ClanMembers",
  "RelicSpawnType": "Plentiful",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "CastleDamageMode": "Never",
  "BloodBoundEquipment": true,
  "CanLootEnemyContainers": false,
  "Death_DurabilityFactorLoss": 0.0,
  "Death_DurabilityLossFactorAsResources": 0.0,
  "AnnounceSiegeWeaponSpawn":  false
}
"""

StandardPvE_Easy = """
{
  "GameModeType": "PvE",
  "DeathContainerPermission": "ClanMembers",
  "RelicSpawnType": "Plentiful",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "CastleDamageMode": "Never",
  "BloodBoundEquipment": true,
  "CanLootEnemyContainers": false,
  "Death_DurabilityFactorLoss": 0.0,
  "Death_DurabilityLossFactorAsResources": 0.0,
  "AnnounceSiegeWeaponSpawn": false,

  "DropTableModifier_General": 1.5,
  "DropTableModifier_Missions": 1.5,
  "MaterialYieldModifier_Global": 1.5,
  "BloodEssenceYieldModifier": 1.5,

  "UnitStatModifiers_Global": {
    "MaxHealthModifier": 0.75,
    "PowerModifier": 0.75
  },
  "UnitStatModifiers_VBlood": {
    "MaxHealthModifier": 0.75,
    "PowerModifier": 0.75
  }
}
"""

StandardPvE_Hard = """
{
  "GameModeType": "PvE",
  "DeathContainerPermission": "ClanMembers",
  "RelicSpawnType": "Plentiful",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "CastleDamageMode": "Never",
  "BloodBoundEquipment": true,
  "CanLootEnemyContainers": false,
  "Death_DurabilityFactorLoss": 0.0,
  "Death_DurabilityLossFactorAsResources": 0.0,
  "AnnounceSiegeWeaponSpawn": false,

  "DropTableModifier_General": 0.75,
  "DropTableModifier_Missions": 0.75,
  "MaterialYieldModifier_Global": 0.75,
  "BloodEssenceYieldModifier": 0.75,

  "UnitStatModifiers_Global": {
    "MaxHealthModifier": 1.25,
    "PowerModifier": 1.25
  },
  "UnitStatModifiers_VBlood": {
    "MaxHealthModifier": 1.25,
    "PowerModifier": 2
  }
}
"""

StandardPvE_Shuffle = """
{
  "GameModeType": "PvE",
  "DeathContainerPermission": "ClanMembers",
  "RelicSpawnType": "Plentiful",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "CastleDamageMode": "Never",
  "BloodBoundEquipment": true,
  "CanLootEnemyContainers": false,
  "Death_DurabilityFactorLoss": 0.0,
  "Death_DurabilityLossFactorAsResources": 0.0,
  "AnnounceSiegeWeaponSpawn": false,
  "RandomizeVBloodSpawnLocations": true,
  "RandomizeVBloodAbilities": true,
  "RandomizeVBloodTechs": true
}
"""

StandardPvP = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",
  "BloodDrainModifier": 1.25,

  "CastleStatModifiers_Global": {
    "CastleLimit": 1
  }
}
"""

StandardPvP_Easy = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",

  "CastleStatModifiers_Global": {
    "CastleLimit": 1
  },

  "DropTableModifier_General": 1.5,
  "DropTableModifier_Missions": 1.5,
  "MaterialYieldModifier_Global": 1.5,
  "BloodEssenceYieldModifier": 1.5,
  "BloodDrainModifier": 1.25,

  "UnitStatModifiers_Global": {
    "MaxHealthModifier": 0.75,
    "PowerModifier": 0.75
  },
  "UnitStatModifiers_VBlood": {
    "MaxHealthModifier": 0.75,
    "PowerModifier": 0.75
  }
}
"""

StandardPvP_Hard = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",

  "CastleStatModifiers_Global": {
    "CastleLimit": 1
  },

  "DropTableModifier_General": 0.75,
  "DropTableModifier_Missions": 0.75,
  "MaterialYieldModifier_Global": 0.75,
  "BloodEssenceYieldModifier": 0.75,
  "BloodDrainModifier": 1.25,

  "UnitStatModifiers_Global": {
    "MaxHealthModifier": 1.25,
    "PowerModifier": 1.25
  },
  "UnitStatModifiers_VBlood": {
    "MaxHealthModifier": 1.25,
    "PowerModifier": 2
  }
}
"""

StandardPvP_Shuffle = """
{
  "GameModeType": "PvP",
  "CastleDamageMode": "TimeRestricted",
  "CastleHeartDamageMode": "CanBeDestroyedOnlyWhenDecaying",
  "DeathContainerPermission": "Anyone",
  "RelicSpawnType": "Unique",
  "BloodBoundEquipment": true,
  "PlayerDamageMode": "Always",
  "PvPProtectionMode": "Short",
  "BloodDrainModifier": 1.25,
  "RandomizeVBloodSpawnLocations": true,
  "RandomizeVBloodAbilities": true,
  "RandomizeVBloodTechs": true,

  "CastleStatModifiers_Global": {
    "CastleLimit": 1
  }
}
"""