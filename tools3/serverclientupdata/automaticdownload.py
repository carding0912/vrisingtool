# -*- coding: utf-8 -*-
import sys
import win32api
from PySide6 import QtWidgets
from PySide6.QtCore import (QMetaObject, QRect,
                            QUrl, QFile, QDir, QSaveFile, Slot, QIODevice, QThread, Qt)
from PySide6.QtGui import QFont, QIcon, QPixmap
from PySide6.QtNetwork import QNetworkAccessManager, QNetworkReply, QNetworkRequest
from PySide6.QtWidgets import (QApplication, QGroupBox, QLabel, QMainWindow,
                               QPushButton, QWidget, QMessageBox, QProgressBar, QFileDialog)
from images import images


class Down_Thread(QThread):
    def __init__(self):
        super(Down_Thread, self).__init__()

    def run(self):
        win32api.ShellExecute(0, 'open', 'down_action.bat', '', 'C:\\steamcmd', 1)


class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        icon = QIcon()
        icon.addPixmap(QPixmap(":/set/set.jpg"), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)

        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setGeometry(QRect(30, 20, 731, 181))
        font = QFont()
        font.setPointSize(13)
        self.groupBox.setFont(font)
        self.groupBox.setStyleSheet(u"color: rgb(170, 0, 0);")

        self.groupBox2 = QGroupBox(self.centralwidget)
        self.groupBox2.setObjectName(u"groupBox")
        self.groupBox2.setGeometry(QRect(30, 240, 731, 320))
        self.groupBox2.setFont(font)
        self.groupBox2.setStyleSheet(u"color: rgb(170, 0, 0);")

        self.pushButton = QPushButton(self.groupBox)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(430, 40, 101, 31))
        font1 = QFont()
        font1.setPointSize(11)
        self.pushButton.setFont(font1)
        self.pushButton.setStyleSheet(u"color: rgb(170, 0, 0);")
        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(110, 40, 231, 31))
        self.label.setFont(font1)
        self.label.setStyleSheet(u"color: rgb(85, 0, 255);")
        self.label2 = QLabel(self.groupBox)
        self.label2.setObjectName(u"label")
        self.label2.setGeometry(QRect(110, 110, 300, 31))
        self.label2.setFont(font1)
        self.label2.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label3 = QLabel(self.groupBox2)
        self.label3.setObjectName(u"label")
        self.label3.setGeometry(QRect(20, 50, 460, 31))
        self.label3.setFont(font1)
        self.label3.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label4 = QLabel(self.groupBox2)
        self.label4.setObjectName(u"label")
        self.label4.setGeometry(QRect(20, 80, 660, 31))
        self.label4.setFont(font1)
        self.label4.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label5 = QLabel(self.groupBox2)
        self.label5.setObjectName(u"label")
        self.label5.setGeometry(QRect(20, 110, 660, 31))
        self.label5.setFont(font1)
        self.label5.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label6 = QLabel(self.groupBox2)
        self.label6.setObjectName(u"label")
        self.label6.setGeometry(QRect(20, 140, 660, 31))
        self.label6.setFont(font1)
        self.label6.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label7 = QLabel(self.groupBox2)
        self.label7.setObjectName(u"label")
        self.label7.setGeometry(QRect(20, 170, 660, 31))
        self.label7.setFont(font1)
        self.label7.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label8 = QLabel(self.groupBox2)
        self.label8.setObjectName(u"label")
        self.label8.setGeometry(QRect(20, 200, 660, 31))
        self.label8.setFont(font1)
        self.label8.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label9 = QLabel(self.groupBox2)
        self.label9.setObjectName(u"label")
        self.label9.setGeometry(QRect(20, 220, 660, 31))
        self.label9.setFont(font1)
        self.label9.setStyleSheet(u"color: rgb(85, 0, 255);")


        self.pushButton_3 = QPushButton(self.groupBox2)
        self.pushButton_3.setObjectName(u"pushButton_2")
        self.pushButton_3.setGeometry(QRect(320, 270, 101, 31))
        self.pushButton_3.setFont(font1)

        self.pushButton_2 = QPushButton(self.groupBox)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setGeometry(QRect(430, 110, 101, 31))
        self.pushButton_2.setFont(font1)
        self.pushButton_2.setDisabled(True)
        self.progress_bar = QProgressBar(self.groupBox)
        self.progress_bar.setGeometry(110, 73, 465, 31)
        self.progress_bar.setFont(font1)
        self.file = None
        self.reply = None

        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)
        self.manager = QNetworkAccessManager(self.centralwidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.pushButton.clicked.connect(self.on_start)
        self.pushButton_2.clicked.connect(self.del_file)
        self.pushButton_3.clicked.connect(self.down_gameserver)

    def down_gameserver(self):
        # self.close()
        str_action = 'steamcmd +login anonymous +app_update 1829350 validate +quit'
        with open('C:/steamcmd/down_action.bat', 'w', encoding='utf-8') as da:
            da.write(str_action)
        self.pushButton_3.setDisabled(True)
        self.infobox = QMessageBox.information(None, '下载提示:', '服务端下载将开启,具体进度看命令提示窗口,完成后会自动退出!可最小化'
                                    '设置其它游戏选项,命令窗口长时间没有交互信息或出现报错可再次点击自动下载按钮进行下载,直至自动关闭')
        self.down_thread = Down_Thread()
        self.down_thread.start()

    def del_file(self):
        if QFile.exists('C:\\steamcmd.zip'):
            QFile.remove('C:\\steamcmd.zip')
            self.pushButton_2.setDisabled(True)

    @Slot()
    def on_start(self):
        """When user press start button"""
        url_file = QUrl("https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip")
        # destination file
        dest_path = QDir.fromNativeSeparators('C:\\'.strip())
        dest_file = QDir(dest_path).filePath(url_file.fileName())
        # Ask a question if file already exists
        if QFile.exists(dest_file):
            ret = QMessageBox.question(None, "文件已存在", "已有steamcmd文件存在,是否覆盖 ?",
                                       QMessageBox.Yes | QMessageBox.No)
            if ret == QMessageBox.No:
                return
            QFile.remove(dest_file)
        self.pushButton.setDisabled(True)
        # Create the file in write mode to append bytes
        self.file = QSaveFile(dest_file)
        if self.file.open(QIODevice.WriteOnly):
            # Start a GET HTTP request
            self.reply = self.manager.get(QNetworkRequest(url_file))
            self.reply.downloadProgress.connect(self.on_progress)
            self.reply.finished.connect(self.on_finished)
            self.reply.readyRead.connect(self.on_ready_read)
            self.reply.errorOccurred.connect(self.on_error)
        else:
            error = self.file.errorString()
            print(f"Cannot open device: {error}")

    @Slot()
    def on_abort(self):
        """When user press abort button"""
        if self.reply:
            self.reply.abort()
            self.progress_bar.setValue(0)

        if self.file:
            self.file.cancelWriting()

        self.pushButton.setDisabled(False)

    @Slot()
    def on_ready_read(self):
        """ Get available bytes and store them into the file"""
        if self.reply:
            if self.reply.error() == QNetworkReply.NoError:
                self.file.write(self.reply.readAll())

    @Slot()
    def on_finished(self):
        """ Delete reply and close the file"""
        if self.reply:
            self.reply.deleteLater()

        if self.file:
            self.file.commit()
        self.pushButton.setDisabled(False)
        import zipfile
        with zipfile.ZipFile('C:/steamcmd.zip', 'r') as f:
            f.extractall('C:/steamcmd/')
        self.pushButton_2.setDisabled(False)

    @Slot(int, int)
    def on_progress(self, bytesReceived: int, bytesTotal: int):
        """ Update progress bar"""
        self.progress_bar.setRange(0, bytesTotal)
        self.progress_bar.setValue(bytesReceived)

    @Slot(QNetworkReply.NetworkError)
    def on_error(self, code: QNetworkReply.NetworkError):
        """ Show a message if an error happen """
        if self.reply:
            QMessageBox.warning(self, "出现下载错误", self.reply.errorString())

    @Slot()
    def on_open_folder(self):

        dir_path = QFileDialog.getExistingDirectory(self, "Open Directory", QDir.homePath(), QFileDialog.ShowDirsOnly)

        if dir_path:
            dest_dir = QDir(dir_path)
            # self.dest_box.setText(QDir.fromNativeSeparators(dest_dir.path()))

    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("服务端全自动构建")
        self.groupBox.setTitle("第一步:下载steamcmd:")
        self.groupBox2.setTitle("第二步:初始化游戏服务端(首次使用看清文字说明):")
        self.pushButton.setText("点此开始")
        self.pushButton_3.setText("开始下载")
        self.label.setText("自动下载并解压steamcmd至C盘")
        self.label2.setText("解压后删除原steamcmd压缩文件(节约空间)")
        self.label3.setText("1. 从官方下载游戏服务端,过程稍长,确认下载后可先进行后面设置步骤!")
        self.label4.setText("2. 点击下方开始开载后会有两次窗口弹出,分别是小窗口和CMD交互命令行!")
        self.label5.setText("3. 下载过程中,交互命令行窗口不可以主动关闭,下载完成后会自动关闭!")
        self.label6.setText("4. 不同的服务器下载时间有所区别,可以先进行后面的游戏参数和服务端参数设置!")
        self.label7.setText("5. 弹出的小确认提示信息窗口看清文字说明后可以主动关闭,服务端全自动构建窗口也可以关闭!")
        self.label8.setText("6. 在对游戏参数设置、服务端参数设置和服务端启动完成前,不要关闭工具主窗口.否则会致使保存数")
        self.label9.setText("    据无法生效!")
        self.pushButton_2.setText("点此删除")
    # retranslateUi


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程
