# -*- coding: utf-8 -*-
import sys
import win32api
from PySide6 import QtWidgets
from PySide6.QtCore import QMetaObject, QRect, QThread, Qt
from PySide6.QtGui import QFont, QIcon, QPixmap
from PySide6.QtWidgets import QApplication, QGroupBox, QLabel, QMainWindow,QPushButton, QWidget, QMessageBox
from images import images


class Down_Thread(QThread):
    def __init__(self):
        super(Down_Thread, self).__init__()

    def run(self):
        win32api.ShellExecute(0, 'open', 'down_action.bat', '', 'C:\\steamcmd', 1)


class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        icon = QIcon()
        icon.addPixmap(QPixmap(":/set/set.jpg"), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)

        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setGeometry(QRect(30, 70, 731, 320))
        font = QFont()
        font.setPointSize(13)

        self.groupBox.setFont(font)
        self.groupBox.setStyleSheet(u"color: rgb(170, 0, 0);")

        font1 = QFont()
        font1.setPointSize(11)

        self.label1 = QLabel(self.groupBox)
        self.label1.setObjectName(u"label")
        self.label1.setGeometry(QRect(20, 50, 460, 31))
        self.label1.setFont(font1)
        self.label1.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label2 = QLabel(self.groupBox)
        self.label2.setObjectName(u"label")
        self.label2.setGeometry(QRect(20, 80, 660, 31))
        self.label2.setFont(font1)
        self.label2.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label3 = QLabel(self.groupBox)
        self.label3.setObjectName(u"label")
        self.label3.setGeometry(QRect(20, 110, 660, 31))
        self.label3.setFont(font1)
        self.label3.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label4 = QLabel(self.groupBox)
        self.label4.setObjectName(u"label")
        self.label4.setGeometry(QRect(20, 140, 660, 31))
        self.label4.setFont(font1)
        self.label4.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label5 = QLabel(self.groupBox)
        self.label5.setObjectName(u"label")
        self.label5.setGeometry(QRect(20, 170, 660, 31))
        self.label5.setFont(font1)
        self.label5.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.label6 = QLabel(self.groupBox)
        self.label6.setObjectName(u"label")
        self.label6.setGeometry(QRect(20, 200, 660, 31))
        self.label6.setFont(font1)
        self.label6.setStyleSheet(u"color: rgb(85, 0, 255);")

        self.pushButton = QPushButton(self.groupBox)
        self.pushButton.setObjectName(u"pushButton_2")
        self.pushButton.setGeometry(QRect(320, 270, 101, 31))
        self.pushButton.setFont(font1)

        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setCentralWidget(self.centralwidget)
        self.pushButton.clicked.connect(self.down_gameserver)

    def down_gameserver(self):
        # self.close()
        str_action = 'steamcmd +login anonymous +app_update 1829350 validate +quit'
        with open('C:/steamcmd/down_action.bat', 'w', encoding='utf-8') as da:
            da.write(str_action)
        self.pushButton.setDisabled(True)
        self.infobox = QMessageBox.information(None, '更新提示:', '      服务端更新将开启,具体进度看命令提示窗口,完成后会自动退出!'
                                               '可最小化命令窗口长时间没有交互信息或出现报错可再次打开工具更新窗口点击自动更新按钮进行更新,直至自动关闭')
        self.down_thread = Down_Thread()
        self.down_thread.start()

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("服务端全自动更新")
        self.groupBox.setTitle("游戏更新(首次使用看清文字说明):")
        self.pushButton.setText("开始更新")
        self.label1.setText("1. 从官方更新游戏服务端,更新前请做好游戏数据备份.")
        self.label2.setText("2. 点击下方开始更新后会有两次窗口弹出,分别是小窗口和CMD交互命令行!")
        self.label3.setText("3. 更新下载过程中,交互命令行窗口不可以主动关闭,下载完成后会自动关闭!")
        self.label4.setText("4. 不同的服务器更新下载时间有所区别,需要稍作等待!")
        self.label5.setText("5. 弹出的小确认提示信息窗口看清文字说明后可以主动关闭,工具的服务端更新分窗口也可以关闭!")
        self.label6.setText("6. 在服务端启动前,不要关闭工具主窗口.否则会致使更新数据无法生效!")
    # retranslateUi

if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程
