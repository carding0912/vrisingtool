# -*- coding: utf-8 -*-
import sys

import win32api
from PySide6.QtCore import QMetaObject, QRect, QSize, Qt, QThread, QFile, QDir
from PySide6.QtGui import QFont, QIcon
from PySide6.QtWidgets import QApplication, QGroupBox, QLabel, QMainWindow, QPushButton, QWidget, QMessageBox


from images import images


class Down_Thread(QThread):
    def __init__(self):
        super(Down_Thread, self).__init__()

    def run(self):
        win32api.ShellExecute(0, 'open', 'start_server.bat', '',
                              'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer', 1)


class Ui_MainWindow(QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(600, 400)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        font = QFont()
        font.setBold(False)
        MainWindow.setFont(font)
        icon = QIcon()
        icon.addFile(u":/set/set.jpg", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet(u"color: rgb(85, 85, 0);")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setGeometry(QRect(30, 20, 540, 360))
        font1 = QFont()
        font1.setPointSize(14)
        font1.setBold(True)
        self.groupBox.setFont(font1)
        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(50, 75, 280, 30))
        font2 = QFont()
        font2.setPointSize(13)
        self.label.setFont(font2)
        self.label.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.pushButton = QPushButton(self.groupBox)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(350, 75, 100, 30))
        font3 = QFont()
        font3.setPointSize(11)
        self.pushButton.setFont(font3)
        self.pushButton.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.pushButton.clicked.connect(self.importdate)

        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(50, 180, 200, 30))
        self.label_2.setFont(font2)
        self.label_2.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.pushButton_2 = QPushButton(self.groupBox)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setGeometry(QRect(290, 180, 100, 30))
        self.pushButton_2.setFont(font3)
        self.pushButton_2.setStyleSheet(u"color: rgb(0, 0, 255);")
        self.pushButton_2.clicked.connect(self.clint_start)
        MainWindow.setCentralWidget(self.centralwidget)
        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)

    def importdate(self):
        self.pushButton.setDisabled(True)
        select = QMessageBox.question(self, '设置导入确认', '确认完成所有设置,并确认数据导入!',
                                      QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
        if select == QMessageBox.StandardButton.Yes:
            try:
                with open('title.txt', 'r', encoding='utf-8') as titleget:
                    serverName = titleget.read()
                with open('savename.txt', 'r', encoding='utf-8') as savenameget:
                    saveName = savenameget.read()

                if serverName and saveName:
                    str_server_start = f"""
@echo off
REM Copy this script to your own file and modify to your content. This file can be overwritten when updating.
set SteamAppId=1604030
echo "Starting V Rising Dedicated Server - PRESS CTRL-C to exit"

@echo on
VRisingServer.exe -persistentDataPath .\save-data -serverName "{serverName}" -saveName "{saveName}" -logFile ".\logs\VRisingServer.log"
"""
                    with open('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\start_server.bat',
                              'w', encoding='utf-8') as f_start:
                        f_start.write(str_server_start)
                    path = 'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\'
                    self.dir = QDir('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\')

                    if not self.dir.exists(path):
                        self.dir.mkdir(path)
                    QFile.remove('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\VRisingServer_Data\\StreamingAssets\\Settings\\ServerHostSettings.json')
                    QFile.remove('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\ServerHostSettings.json')
                    QFile.remove('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\ServerGameSettings.json')
                    QFile.remove('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\VRisingServer_Data\\StreamingAssets\Settings\\ServerGameSettings.json')

                    QFile.copy('ServerHostSettings.json',
                               'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\ServerHostSettings.json')
                    QFile.copy('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\ServerHostSettings.json',
                               'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\VRisingServer_Data\\StreamingAssets\\Settings\\ServerHostSettings.json')
                    QFile.copy('ServerGameSettings.json',
                               'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\save-data\\ServerGameSettings.json')
                    QFile.copy('ServerGameSettings.json',
                               'C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\VRisingServer_Data\\StreamingAssets\Settings\\ServerGameSettings.json')
                    self.box = QMessageBox.information(self, '导入成功:', '游戏参数和服务器参数导入成功!',
                                                       QMessageBox.StandardButton.Ok)
                    self.pushButton.setDisabled(False)

            except:
                self.waringbox = QMessageBox.warning(self, '导入报错:', '服务器参数没有设置或设置后没有保存.',
                                                     QMessageBox.StandardButton.Yes)
                return

        else:
            self.pushButton.setDisabled(False)

    def clint_start(self):
        if QFile.exists('C:\\steamcmd\\steamapps\\common\\VRisingDedicatedServer\\start_server.bat'):
            self.pushButton_2.setDisabled(True)
            self.infobox = QMessageBox.information(self, '启动提示:',
                                                   '服务端已启动,稍后即可以进入游戏,开服过程中,交互窗口不能关闭!')
            self.down_thread = Down_Thread()
            self.down_thread.start()
        else:
            self.infobox = QMessageBox.information(self, '启动报错:',
                                                   '请先完成此窗口第一步操作,然后再启动服务端!')

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("服务端启动")
        self.groupBox.setTitle("服务端启动:")
        self.label.setText("一、导入游戏参数和服务器参数配置:")
        self.pushButton.setText("点击自动导入")
        self.label_2.setText("二、启动服务端:")
        self.pushButton_2.setText("点此启动")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程
