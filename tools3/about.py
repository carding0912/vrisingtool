import sys
from PySide6.QtCore import QMetaObject,QRect
from PySide6.QtGui import QFont, Qt
from PySide6.QtWidgets import QLabel, QMainWindow, QWidget, QApplication

class Ui_MainWindow(QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)  # 初始化窗体设置

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(469, 291)
        MainWindow.setWindowFlags(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        font = QFont()
        font.setFamilies([u"\u534e\u6587\u884c\u6977"])
        MainWindow.setFont(font)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(100, 0, 281, 51))
        font1 = QFont()
        font1.setFamilies([u"\u534e\u6587\u884c\u6977"])
        font1.setPointSize(16)
        self.label.setFont(font1)
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(40, 190, 401, 21))
        font2 = QFont()
        font2.setFamilies([u"\u5fae\u8f6f\u96c5\u9ed1"])
        font2.setPointSize(11)
        self.label_2.setFont(font2)
        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(40, 230, 411, 21))
        font3 = QFont()
        font3.setFamilies([u"\u5fae\u8f6f\u96c5\u9ed1"])
        font3.setPointSize(9)
        self.label_3.setFont(font3)
        self.label_4 = QLabel(self.centralwidget)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(5, 40, 470, 41))
        font4 = QFont()
        font4.setFamilies([u"\u534e\u6587\u884c\u6977"])
        font4.setPointSize(16)
        self.label_4.setFont(font4)
        self.label_5 = QLabel(self.centralwidget)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(40, 80, 191, 31))
        font5 = QFont()
        font5.setFamilies([u"\u5fae\u8f6f\u96c5\u9ed1"])
        font5.setPointSize(10)
        self.label_5.setFont(font5)
        self.label_6 = QLabel(self.centralwidget)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(110, 130, 220, 31))
        self.label_6.setFont(font5)
        self.label_7 = QLabel(self.centralwidget)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setGeometry(QRect(110, 110, 320, 21))
        self.label_7.setFont(font5)
        self.label_8 = QLabel(self.centralwidget)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setGeometry(QRect(110, 160, 220, 21))
        self.label_8.setFont(font5)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("关于我们")
        self.label.setText("始于工具,创造工具,实现工具.")
        self.label_2.setText("本工具开发和实现模式:Python3.9 + Pyside6 + MySQL8.0")
        self.label_3.setText("商用开发许可证: OSI:GUN库或宽通用公共许可证(LGPL):其它专有许可证")
        self.label_4.setText("欢迎各种渠道的合作机会,或者交个朋友共同致富!")
        self.label_5.setText("可合作项目: 1.web前端所有项目;")
        self.label_6.setText("3.各种手机APP,微信小程序等等;")
        self.label_7.setText("2.web后端(基于Python的Django,Flask等相关框架;)")
        self.label_8.setText("4,各种软件或工具的定制和开发.")
    # retranslateUi

if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    MainWindow.show()  # 显示窗体
    sys.exit(app.exec())  # 程序关闭时退出进程